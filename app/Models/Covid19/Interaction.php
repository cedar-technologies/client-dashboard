<?php

namespace App\Models\Covid19;

use Illuminate\Database\Eloquent\Model;

class Interaction extends Model
{
    protected $connection = 'covid19';

    public $timestamps = false;

    protected $fillable = [
        'mobile_no_1',
        'mobile_no_2',
        'longitude',
        'latitude',
        'distance',
        'interaction_started_at',
        'interaction_ended_at'
    ];

    protected $casts = [
        'interaction_started_at'  => 'date:l, F d, Y H:i:s',
    ];
}

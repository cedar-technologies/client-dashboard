<?php

namespace App\Models\Covid19;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class User extends Model
{
    protected $connection = 'covid19';

    protected $fillable = [
        'mobile_no',
        'is_doctor',
        'is_government',
        'is_covid_positive',
        'declared_covid_positive_at',
        'notify'
    ];
    
    protected $casts = [
        'declared_covid_positive_at'  => 'date:l, F d, Y H:i:s',
    ];
}

<?php

namespace App\Models\Covid19;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $connection = 'covid19';

    public $timestamps = false;

    protected $fillable = [
        'mobile_no',
        'longitude',
        'latitude',
        'sublocation',
        'timestamps',
    ];
}

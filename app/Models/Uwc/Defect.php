<?php

namespace App\Models\Uwc;

use Illuminate\Database\Eloquent\Model;

class Defect extends Model
{
    protected $connection = 'uwc';

    public $table = 'defects';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id',
        'production_line_no',
        'station_no',
        'object_type',
        'traceability_no',
        'units',
        'pfr_no',
        'reason',
        'batch_id'
    ];

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('D, M d, Y');
    }
}

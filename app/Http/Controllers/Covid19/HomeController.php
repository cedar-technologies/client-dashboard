<?php

namespace App\Http\Controllers\Covid19;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        session(['active_nav' => 'dashboard']);

        return view('covid19.layouts.dashboard');
    }

    public function userList()
    {
        session(['active_nav' => 'users']);

        return view('covid19.layouts.users-list');
    }

    public function userInteractionList(Request $request)
    {
        session(['active_nav' => 'user-interactions']);
        $notification_mobile_no = $request->mobile_no;

        return view('covid19.layouts.user-interactions-list', compact('notification_mobile_no'));
    }

    public function infectionTree()
    {
        session(['active_nav' => 'infection-tree']);

        return view('covid19.layouts.infection-tree');
    }

    public function newsFeed()
    {
        session(['active_nav' => 'news-feed']);

        return view('covid19.layouts.news-feed');
    }

    public function hotspotMap()
    {
        session(['active_nav' => 'hotspot-map']);

        return view('covid19.layouts.hotspot-map');
    }

    public function notificationList()
    {
        session(['active_nav' => 'notification-list']);

        return view('covid19.layouts.notification-list');
    }
}

<?php

namespace App\Http\Controllers\Covid19\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Covid19\InteractionService;
use Carbon\Carbon;

class InteractionApiController extends Controller
{
    private $interaction_service;

    public function __construct()
    {
        $this->interaction_service = new InteractionService;
    }

    public function datatableList(Request $request)
    {
        $data = $this->interaction_service->datatableList($request);

        echo $data;
    }
}

<?php

namespace App\Http\Controllers\Covid19\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Covid19\AnalyticsService;

class ChartApiController extends Controller
{
    private $analytics_service;

    public function __construct()
    {
        $this->analytics_service = new AnalyticsService;
    }

    public function getInfectedUsersChartData(Request $request)
    {
        $data = $this->analytics_service->getInfectedUsers($request->filter);
        
        return response()->json(
            [
                'message' => 'success',
                'attributes' => [
                    'year' => $data['year'],
                    'title' => $data['title'],
                    'subtitle' => $data['subtitle'],
                    'x_axis' => $data['x_axis'],
                    'infected_users' => $data['infected_users'],
                    'is_1_month' => $data['is_1_month'] ?? false,
                    'month_year' => $data['month_year'] ?? null
                ]
            ],
            200
        );
    }

}
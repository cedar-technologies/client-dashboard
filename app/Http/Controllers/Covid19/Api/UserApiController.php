<?php

namespace App\Http\Controllers\Covid19\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Covid19\UserService;
use Carbon\Carbon;

class UserApiController extends Controller
{
    private $user_service;

    public function __construct()
    {
        $this->user_service = new UserService;
    }

    public function datatableList(Request $request)
    {
        $data = $this->user_service->datatableList($request);

        echo $data;
    }

    public function getInfectionTreeData(Request $request)
    {
        $data = $this->user_service->getInfectionTreeData($request);

        echo $data;
    }

    public function update(Request $request, $id)
    {
        $data = $this->user_service->update($request, $id);

        return response()->json($data, 200);
    }
}

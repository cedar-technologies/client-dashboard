<?php

namespace App\Http\Controllers\Covid19\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Covid19\AnalyticsService;

class StatisticApiController extends Controller
{
    private $analytics_service;

    public function __construct()
    {
        $this->analytics_service = new AnalyticsService;
    }

    public function getHeaderData()
    {
        $data = $this->analytics_service->getHeaderData();
        
        return response()->json(
            [
                'message' => 'success',
                'attributes' => $data
            ],
            200
        );
    }

    public function getUnitsCompletedChartData(Request $request)
    {
        $data = $this->analytics_service->getUnitsCompleted($request->production_line, $request->filter);

        return response()->json(
            [
                'message' => 'success',
                'attributes' => [
                    'year' => $data['year'],
                    'title' => $data['title'],
                    'subtitle' => $data['subtitle'],
                    'x_axis' => $data['x_axis'],
                    'station1' => $data['station1'],
                    'station2' => $data['station2'],
                    'station3' => $data['station3'],
                    'is_1_month' => $data['is_1_month'] ?? false,
                    'month_year' => $data['month_year'] ?? null
                ]
            ],
            200
        );
    }

    public function getDefectiveUnitsChartData(Request $request)
    {
        $data = $this->analytics_service->getDefectiveUnits($request->production_line, $request->filter);

        return response()->json(
            [
                'message' => 'success',
                'attributes' => [
                    'year' => $data['year'],
                    'title' => $data['title'],
                    'subtitle' => $data['subtitle'],
                    'x_axis' => $data['x_axis'],
                    'defects' => $data['defects'],
                    'is_1_month' => $data['is_1_month'] ?? false,
                    'month_year' => $data['month_year'] ?? null
                    ]
                ],
                200
            );
    }
}
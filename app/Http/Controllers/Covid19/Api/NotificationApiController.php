<?php

namespace App\Http\Controllers\Covid19\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Covid19\UserService;
use Carbon\Carbon;

class NotificationApiController extends Controller
{
    private $user_service;

    public function __construct()
    {
        $this->user_service = new UserService;
    }

    public function datatableList(Request $request)
    {
        $data = $this->user_service->notificationDatatableList($request);

        echo $data;
    }

    public function updateInteractedUserNotifyFlag(Request $request)
    {
        $data = $this->user_service->updateInteractedUserNotifyFlag($request);

        return response()->json($data, 200);
    }
}

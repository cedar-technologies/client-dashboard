<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RootController extends Controller
{
    public function index()
    {
        if ($user = \Auth::user()) {
            return redirect()->route($user->project . '.home');
        }

        return redirect()->route('login');
    }
}

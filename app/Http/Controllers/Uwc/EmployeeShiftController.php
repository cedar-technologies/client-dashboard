<?php

namespace App\Http\Controllers\Uwc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmployeeShiftController extends Controller
{
    public function list()
    {
        session(['active_nav' => 'employee-shifts']);

        return view('uwc.layouts.employee-shifts.list');
    }
}

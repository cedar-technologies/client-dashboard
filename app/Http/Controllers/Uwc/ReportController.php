<?php

namespace App\Http\Controllers\Uwc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Uwc\ExcelService;
use App\Models\Uwc\Station1Job;
use Carbon\Carbon;
use Validator;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['active_nav' => 'reports']);

        return view('uwc.layouts.reports.index');
    }

    /**
     * Generate excel file and download.
     *
     * @return \Illuminate\Http\Response
     */
    public function download(Request $request)
    {
        $excel_service = new ExcelService;
        $validation = Validator::make(
            $request->all(),
            [
                'production_line_no' => 'required',
                'station_no' => 'required',
                'start_date' => 'required',
                'end_date' => 'required'
            ]
        );

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        
        $request->start_date ? $request->start_date = Carbon::parse($request->start_date)->format('Y-m-d H:i:s') : null;
        $request->end_date ? $request->end_date = Carbon::parse($request->end_date)->addDay()->subSecond(1)->format('Y-m-d H:i:s') : null;

        return $excel_service->generateTraceabilityReport($request);
    }
}

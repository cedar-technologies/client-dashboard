<?php

namespace App\Http\Controllers\Uwc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Uwc\AnalyticsService;

class HomeController extends Controller
{
    private $analytics_service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->analytics_service = new AnalyticsService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        session(['active_nav' => 'dashboard']);
        
        return view('uwc.layouts.dashboard');
    }
}

<?php

namespace App\Http\Controllers\Uwc;

use App\Http\Controllers\Controller;
use App\Models\Uwc\Employee;
use Illuminate\Http\Request;
use Validator;

class EmployeeController extends Controller
{
    public function list(Request $request)
    {
        session(['active_nav' => 'employees']);

        $employee_list = Employee::get();
        $is_admin = auth()->user()->isAdmin();

        if ($is_admin) {
            $is_admin = "true";
        } else {
            $is_admin = "false";
        }
        
        return view('uwc.layouts.employees.list', compact('employee_list'))->with('is_admin', $is_admin);
    }

    public function createView(Request $request)
    {
        session(['active_nav' => 'employees']);

        $employee_count = Employee::count();

        if ($employee_count == config('staticdata.employee_limit')) {
            return redirect()->route('uwc.employees.view.list');
        }

        return view('uwc.layouts.employees.create');
    }

    public function create(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'employee_id' => 'required|max:255|unique:uwc.employees,employee_id,NULL,id,uwc.employees.deleted_at,NULL',
                'name'        => 'required|between:2,100',
                'card_no'     => 'required|max:20'
            ]
        );

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        
        Employee::create(
            [
                'employee_id' => $request->employee_id,
                'name'        => $request->name,
                'card_no'     => $request->card_no
            ]
        );

        return redirect()->route('uwc.employees.view.list');
    }

    public function edit(Request $request, $id)
    {
        session(['active_nav' => 'employees']);
        
        $employee = Employee::find($id);

        return view('uwc.layouts.employees.edit')->with('employee', $employee);
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'employee_id' => 'required|max:255',
                'name'        => 'required|between:2,100',
                'card_no'     => 'required|max:20'
            ]
        );

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        Employee::find($id)->update(
            [
                'employee_id' => $request->employee_id,
                'name'        => $request->name,
                'card_no'     => $request->card_no
            ]
        );

        return redirect()->route('uwc.employees.view.list');
    }
    
    public function delete(Request $request, $id)
    {
        Employee::where('employee_id', $id)->delete();

        return redirect()->route('uwc.employees.view.list');
    }
}

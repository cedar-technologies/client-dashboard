<?php

namespace App\Http\Controllers\Uwc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        session(['active_nav' => 'traceabilities']);

        return view('uwc.layouts.stations.list');
    }
}

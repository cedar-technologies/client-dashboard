<?php

namespace App\Http\Controllers\Uwc\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Uwc\Employee;
use App\Services\Uwc\EmployeeService;
use Carbon\Carbon;

class EmployeeManagementApiController extends Controller
{
    private $employee_service;

    public function __construct()
    {
        $this->employee_service = new EmployeeService;
    }

    public function datatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        if ($draw == 1 || $request['order'][0]['column'] == 3) {
            $sort = 'created_at';
            $order = 'desc';
        } else {
            $sort = config('staticdata.employees.table-header.' . $request['order'][0]['column']);
            $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';
        }

        $total = Employee::when($request->search_by, function($q) use ($request) {
            return $q->whereRaw("$request->search_by LIKE '%". strtoupper($request->keyword) . "%'");
        })->count();

        $employee_list = Employee::select(
            'id',
            'employee_id',
            'name',
            'card_no')
            ->when($request->search_by, function($q) use ($request) {
                return $q->whereRaw("$request->search_by LIKE '%". strtoupper($request->keyword) . "%'");
            })
            ->orderBy($sort, $order)
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($employee_list as $employee) {
            $employee->total_working_hours = $this->employee_service->formatInHours($this->employee_service->getTotalWorkingHours($employee->employee_id));
            if ($request->is_admin) {
                $employee->delete = '<a class="delete-icon" href="#delete_modal" data-toggle="modal" data-target="#delete_modal" ' .
                                        'data-employee_id="' . $employee->employee_id . '">' .
                                        '<i class="fas fa-trash-alt"></i>' .
                                    '</a>';
                $employee->employee_id = '<a href="' . route("uwc.employees.view.edit", ["id" => $employee->id]) . '">' .
                                            $employee->employee_id .
                                         '</a>';
            }
        }

        $employee_list = $employee_list->toArray();

        // if ($request['order'][0]['column'] == 3) {
        //     usort($employee_list, function($a, $b) use ($request) {
        //         if ($request['order'][0]['dir'] == 'asc') {
        //             return $a['total_working_hours'] - $b['total_working_hours'];
        //         } else {
        //             return $b['total_working_hours'] - $a['total_working_hours'];
        //         }
        //     });
        // }

        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $employee_list,
        ];

        echo json_encode($data, JSON_PRETTY_PRINT);
    }
}

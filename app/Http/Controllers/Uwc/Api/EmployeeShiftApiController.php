<?php

namespace App\Http\Controllers\Uwc\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Uwc\EmployeeShift;
use App\Models\Uwc\Employee;
use Carbon\Carbon;

class EmployeeShiftApiController extends Controller
{
    public function datatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        if ($draw == 1) {
            $sort = 'clock_in_datetime';
            $order = 'desc';
        } else {
            $sort = config('staticdata.employee_shifts.table-header.' . $request['order'][0]['column']);
            $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';
        }

        $request->start_date ? $request->start_date = Carbon::parse($request->start_date)->format('Y-m-d H:i:s') : null;
        $request->end_date ? $request->end_date = Carbon::parse($request->end_date)->addDay()->subSecond(1)->format('Y-m-d H:i:s') : null;

        $total = EmployeeShift::when($request->production_line_no != 'all', function($q) use ($request) {
                return $q->where('production_line_no', $request->production_line_no);
            })
            ->when($request->station_no != 'all', function($q) use ($request) {
                return $q->where('station_no', $request->station_no);
            })
            ->where('clock_out_datetime', '!=', 'NULL')
            ->when($request->start_date && $request->end_date, function($q) use ($request) {
                return $q->whereBetween('clock_out_datetime', [$request->start_date, $request->end_date]);
            })
            ->when($request->search_by, function($q) use ($request) {
                return $q->whereRaw("$request->search_by LIKE '%". strtoupper($request->keyword) . "%'");
            })->count();

        $shift_list = EmployeeShift::select(
            'employee_id',
            'pfr_no',
            'production_line_no',
            'station_no',
            'clock_in_datetime',
            'clock_out_datetime',
            'working_hours')
            ->when($request->production_line_no != 'all', function($q) use ($request) {
                return $q->where('production_line_no', $request->production_line_no);
            })
            ->when($request->station_no != 'all', function($q) use ($request) {
                return $q->where('station_no', $request->station_no);
            })
            ->where('clock_out_datetime', '!=', 'NULL')
            ->when($request->start_date && $request->end_date, function($q) use ($request) {
                return $q->whereBetween('clock_out_datetime', [$request->start_date, $request->end_date]);
            })
            ->when($request->search_by, function($q) use ($request) {
                return $q->whereRaw("$request->search_by LIKE '%". strtoupper($request->keyword) . "%'");
            })
            ->orderBy($sort, $order)
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($shift_list as $shift) {
            $shift->clock_in_datetime = Carbon::parse($shift->clock_in_datetime)->toDayDateTimeString();
            $shift->clock_out_datetime = Carbon::parse($shift->clock_out_datetime)->toDayDateTimeString();
            $shift->working_hours = number_format($shift->working_hours / 60, 2, '.', ',');
        }

        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $shift_list,
        ];

        echo json_encode($data, JSON_PRETTY_PRINT);
    }
}

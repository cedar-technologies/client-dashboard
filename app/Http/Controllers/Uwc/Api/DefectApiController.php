<?php

namespace App\Http\Controllers\Uwc\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Uwc\Defect;
use Carbon\Carbon;

class DefectApiController extends Controller
{
    public function datatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        if ($draw == 1) {
            $sort = 'created_at';
            $order = 'desc';
        } else {
            $sort = config('staticdata.defects.table-header.' . $request['order'][0]['column']);
            $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';
        }

        $request->start_date ? $request->start_date = Carbon::parse($request->start_date)->format('Y-m-d H:i:s') : null;
        $request->end_date ? $request->end_date = Carbon::parse($request->end_date)->addDay()->subSecond(1)->format('Y-m-d H:i:s') : null;

        $total = Defect::when($request->production_line_no != 'all', function($q) use ($request) {
                return $q->where('production_line_no', $request->production_line_no);
            })
            ->when($request->station_no != 'all', function($q) use ($request) {
                return $q->where('station_no', $request->station_no);
            })
            ->when($request->start_date && $request->end_date, function($q) use ($request) {
                return $q->whereBetween('created_at', [$request->start_date, $request->end_date]);
            })
            ->when($request->search_by, function($q) use ($request) {
                return $q->whereRaw("$request->search_by LIKE '%". strtoupper($request->keyword) . "%'");
            })->count();
            
        $defect_list = Defect::select(
            'pfr_no',
            'production_line_no',
            'station_no',
            'object_type',
            'traceability_no',
            'employee_id',
            'created_at',
            'units',
            'reason')
            ->when($request->production_line_no != 'all', function($q) use ($request) {
                return $q->where('production_line_no', $request->production_line_no);
            })
            ->when($request->station_no != 'all', function($q) use ($request) {
                return $q->where('station_no', $request->station_no);
            })
            ->when($request->start_date && $request->end_date, function($q) use ($request) {
                return $q->whereBetween('created_at', [$request->start_date, $request->end_date]);
            })
            ->when($request->search_by, function($q) use ($request) {
                return $q->whereRaw("$request->search_by LIKE '%". strtoupper($request->keyword) . "%'");
            })
            ->orderBy($sort, $order)
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($defect_list as $defect) {
            // $defect->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $defect->created_at)->format('Y-m-d');
            $defect->object_type = ucfirst($defect->object_type);
            if (strlen($defect->traceability_no) > 20) {
                $defect->traceability_no = substr_replace($defect->traceability_no, '-<br>', 19, 0);
            }
        }

        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $defect_list,
        ];

        echo json_encode($data, JSON_PRETTY_PRINT);
    }
}

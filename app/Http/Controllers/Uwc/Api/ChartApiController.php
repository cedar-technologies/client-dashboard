<?php

namespace App\Http\Controllers\Uwc\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Uwc\AnalyticsService;

class ChartApiController extends Controller
{
    private $analytics_service;

    public function __construct()
    {
        $this->analytics_service = new AnalyticsService;
    }

    public function getDailyPercentageChartData(Request $request)
    {
        $data = $this->analytics_service->getDailyPercentage($request->production_line);
        
        return response()->json(
            [
                'message' => 'success',
                'attributes' => [
                    'working_hours' => $data['working_hours'],
                    'total_units' => $data['total_units'],
                    'total_avg_units_per_hour' => $data['total_avg_units_per_hour'],
                    'defects_percentage' => $data['defects_percentage'],
                    'avg_units_per_hour' => config('staticdata.avg_units_per_hour'),
                    'total_production_line' => config('staticdata.production_line_limit'),
                    'total_station' => config('staticdata.station_limit')
                ]
            ],
            200
        );
    }

    public function getUnitsCompletedChartData(Request $request)
    {
        $data = $this->analytics_service->getUnitsCompleted($request->production_line, $request->filter);

        return response()->json(
            [
                'message' => 'success',
                'attributes' => [
                    'year' => $data['year'],
                    'title' => $data['title'],
                    'subtitle' => $data['subtitle'],
                    'x_axis' => $data['x_axis'],
                    'station1' => $data['station1'],
                    'station2' => $data['station2'],
                    'station3' => $data['station3'],
                    'is_1_month' => $data['is_1_month'] ?? false,
                    'month_year' => $data['month_year'] ?? null
                ]
            ],
            200
        );
    }

    public function getDefectiveUnitsChartData(Request $request)
    {
        $data = $this->analytics_service->getDefectiveUnits($request->production_line, $request->filter);

        return response()->json(
            [
                'message' => 'success',
                'attributes' => [
                    'year' => $data['year'],
                    'title' => $data['title'],
                    'subtitle' => $data['subtitle'],
                    'x_axis' => $data['x_axis'],
                    'defects' => $data['defects'],
                    'is_1_month' => $data['is_1_month'] ?? false,
                    'month_year' => $data['month_year'] ?? null
                    ]
                ],
                200
            );
    }
}
<?php

namespace App\Http\Controllers\Uwc\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Uwc\Station2Job;
use Carbon\Carbon;

class Station2JobApiController extends Controller
{
    public function datatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        if ($draw == 1) {
            $sort = 'batch_start_datetime';
            $order = 'desc';
        } else {
            $sort = config('staticdata.station2.table-header.' . $request['order'][0]['column']);
            $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';
        }

        $request->start_date ? $request->start_date = Carbon::parse($request->start_date)->format('Y-m-d H:i:s') : null;
        $request->end_date ? $request->end_date = Carbon::parse($request->end_date)->addDay()->subSecond(1)->format('Y-m-d H:i:s') : null;

        $total = Station2Job::when($request->production_line_no != 'all', function($q) use ($request) {
                return $q->where('production_line_no', $request->production_line_no);
            })
            ->when($request->start_date && $request->end_date, function($q) use ($request) {
                return $q->whereBetween('batch_start_datetime', [$request->start_date, $request->end_date]);
            })
            ->when($request->search_by && $request->search_by != 'pfr_no' && $request->search_by != 'traceability_no', function($q) use ($request) {
                return $q->whereRaw("$request->search_by LIKE '%". strtoupper($request->keyword) . "%'");
            })
            ->when($request->search_by && $request->search_by == 'pfr_no', function($q) use ($request) {
                return $q->whereRaw(
                    "pfr_1_no LIKE '%". strtoupper($request->keyword) . "%' OR " .
                    "pfr_2_no LIKE '%". strtoupper($request->keyword) . "%'"
                );
            })
            ->when($request->search_by && $request->search_by == 'traceability_no', function($q) use ($request) {
                return $q->whereRaw(
                    "body_traceability_no LIKE '%". strtoupper($request->keyword) . "%' OR " .
                    "cap_traceability_no LIKE '%". strtoupper($request->keyword) . "%'"
                );
            })
            ->whereNotNull('batch_end_datetime')
            ->count();

        $job_list = Station2Job::select(
            'employee_id',
            'production_line_no',
            'body_traceability_no',
            'body_subtraceability_no',
            'cap_traceability_no',
            'pfr_1_no',
            'pfr_2_no',
            'batch_start_datetime',
            'batch_end_datetime',
            'completed',
            'incomplete',
            'reason_of_incompletion'
            )
            ->when($request->production_line_no != 'all', function($q) use ($request) {
                return $q->where('production_line_no', $request->production_line_no);
            })
            ->when($request->start_date && $request->end_date, function($q) use ($request) {
                return $q->whereBetween('batch_start_datetime', [$request->start_date, $request->end_date]);
            })
            ->when($request->search_by && $request->search_by != 'pfr_no' && $request->search_by != 'traceability_no', function($q) use ($request) {
                return $q->whereRaw("$request->search_by LIKE '%". strtoupper($request->keyword) . "%'");
            })
            ->when($request->search_by && $request->search_by == 'pfr_no', function($q) use ($request) {
                return $q->whereRaw(
                    "pfr_1_no LIKE '%". strtoupper($request->keyword) . "%' OR " .
                    "pfr_2_no LIKE '%". strtoupper($request->keyword) . "%'"
                );
            })
            ->when($request->search_by && $request->search_by == 'traceability_no', function($q) use ($request) {
                return $q->whereRaw(
                    "body_traceability_no LIKE '%". strtoupper($request->keyword) . "%' OR " .
                    "cap_traceability_no LIKE '%". strtoupper($request->keyword) . "%'"
                );
            })
            ->whereNotNull('batch_end_datetime')
            ->orderBy($sort, $order)
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($job_list as $job) {
            $job->batch_start_datetime = Carbon::parse($job->batch_start_datetime)->toDayDateTimeString();
            $job->batch_end_datetime = Carbon::parse($job->batch_end_datetime)->toDayDateTimeString();
            if (strlen($job->cap_traceability_no) > 20) {
                $job->body_traceability_no = substr_replace($job->body_traceability_no, '-<br>', 19, 0);
            }
        }

        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $job_list,
        ];

        echo json_encode($data, JSON_PRETTY_PRINT);
    }
}

<?php
 
namespace App\Services\Covid19;

use Illuminate\Http\Request;
use App\Models\Covid19\Interaction;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Facades\DB;

class InteractionService
{ 
    public function datatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        if ($draw == 1) {
            $sort = 'interaction_started_at';
            $order = 'desc';
        } else {
            $sort = config('staticdata.covid19.user-interactions.table-header.' . $request['order'][0]['column']);
            $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';
        }

        $total = Interaction::select(
            'id',
            'mobile_no_1',
            'mobile_no_2',
            DB::raw('ROUND(AVG(distance)) as distance'),
            'interaction_started_at'
            )->when($request->start_date && $request->end_date, function($q) use ($request) {
                $start_date = Carbon::parse($request->start_date)->toDateTimeString();
                $end_date = Carbon::parse($request->end_date)->addDay()->subSecond()->toDateTimeString();

                return $q->whereBetween('interaction_started_at', [$start_date, $end_date]);
            })
            ->when($request->distance || $request->distance == 0, function($q) use ($request) {
                return $q->whereBetween('min_distance', [0, $request->distance]);
            })
            ->where(
                function($q) use ($request) {
                    return $q->where('mobile_no_1', $request->keyword)
                        ->orWhere('mobile_no_2', $request->keyword);
                }
            )
            ->groupBy('mobile_no_1')
            ->get()->toArray();
        
        $interaction_list = Interaction::select(
            'id',
            'mobile_no_1',
            'mobile_no_2',
            DB::raw('ROUND(latitude, 6) as latitude'),
            DB::raw('ROUND(longitude, 6) as longitude'),
            DB::raw('ROUND(AVG(distance), 2) as distance'),
            DB::raw('MIN(ROUND(min_distance, 2)) as min_distance'),
            DB::raw('MAX(ROUND(min_distance, 2)) as max_distance'),
            'interaction_started_at',
            DB::raw('SUM(TIMESTAMPDIFF(SECOND, interaction_started_at, interaction_ended_at)) as duration')
            )->when($request->start_date && $request->end_date, function($q) use ($request) {
                $start_date = Carbon::parse($request->start_date)->toDateTimeString();
                $end_date = Carbon::parse($request->end_date)->addDay()->subSecond()->toDateTimeString();

                return $q->whereBetween('interaction_started_at', [$start_date, $end_date]);
            })
            ->when($request->distance || $request->distance == 0, function($q) use ($request) {
                return $q->whereBetween('min_distance', [0, $request->distance]);
            })
            ->where(
                function($q) use ($request) {
                    return $q->where('mobile_no_1', $request->keyword)
                        ->orWhere('mobile_no_2', $request->keyword);
                }
            )
            ->groupBy('mobile_no_1')
            ->orderBy($sort, $order)
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($interaction_list as $interaction) {
            if ($interaction->mobile_no_1 == $request->keyword) {
                $interaction->mobile_no = $interaction->mobile_no_2;
            } else {
                $interaction->mobile_no = $interaction->mobile_no_1;
            }
            $interaction->location = $interaction->latitude.', '.$interaction->longitude;
            $interaction->duration = CarbonInterval::seconds($interaction->duration)->cascade()->forHumans();
            unset($interaction->latitude, $interaction->longitude);
        }

        $interaction_list = $interaction_list->toArray();
        $total = count($total);
        
        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $interaction_list,
        ];

        return json_encode($data, JSON_PRETTY_PRINT);
    }
}
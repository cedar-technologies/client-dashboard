<?php

namespace App\Services\Covid19;

use Illuminate\Http\Request;
use App\Models\Covid19\User;
use App\Models\Covid19\Interaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AnalyticsService
{
    public function getHeaderData()
    {
        $total_infected_users = $this->getTotalInfectedUsers();
        $total_user_interactions = $this->getTotalUserInteractions();
        if ($total_user_interactions != 0) {
            $percentage_interaction_induced_infections = $total_infected_users / $total_user_interactions * 100;
        } else {
            $percentage_interaction_induced_infections = 0;
        }

        $result = [
            'total_infected_users' => $total_infected_users,
            'total_user_interactions' => $total_user_interactions,
            'percentage_interaction_induced_infections' => round($percentage_interaction_induced_infections, 2, PHP_ROUND_HALF_UP)
        ];

        return $result;
    }
     
    public function getInfectedUsers($filter = 'yearly')
    {
        $year = Carbon::now()->year;

        switch($filter) {
            case 'yearly':
                $data = $this->getInfectedUsersYearly($year);
                $title = "Infected users chart in $year";
                $subtitle = "Number of units completed each month in current year";
                break;

            case 'weekly':
                $data = $this->getInfectedUsersWeekly($year);
                $title = "Infected users chart in past 1 week";
                $subtitle = "Number of units completed each day in past 1 week";
                break;

            case 'hourly':
                $data = $this->getInfectedUsersHourly($year);
                $title = "Infected users chart in past 24 hours";
                $subtitle = "Number of units completed each hour in past 24 hours";
                break;
        }

        return [
            'year' => $year,
            'title' => $title,
            'subtitle' => $subtitle,
            'x_axis' => $data['x_axis'],
            'infected_users' => $data['infected_users'],
            'is_1_month' => $data['is_1_month'] ?? false,
            'month_year' => $data['month_year'] ?? null
        ];
    }

    private function getTotalInfectedUsers()
    {
        return User::where('is_covid_positive', true)->count();
    }

    private function getTotalUserInteractions()
    {
        return Interaction::count();
    }

    private function getInfectedUsersYearly($year)
    {
        $infected_users = User::select(
                DB::raw('COUNT(id) as total_infected_users'),
                DB::raw('MONTH(declared_covid_positive_at) as month')
            )
            ->whereNotNull('declared_covid_positive_at')
            ->where('is_covid_positive', true)
            ->whereYear('declared_covid_positive_at', $year)
            ->groupBy('month')
            ->orderBy('month', 'asc')
            ->get();

        $infected_users_count_month = 1;
        $temp_month = 0;

        if (!$infected_users->isEmpty()) {
            $i = 0;
            foreach ($infected_users as $user) {
                if ($i != 0 && $temp_month != $user->month - 1) {
                    $infected_users_count_month++;
                    $temp_month = $user->month - 1;
                }
                if ($i == 0) {
                    $temp_month = $user->month - 1;
                }
                $infected_users_ordered[$i]['month'] = $user->month - 1;
                $infected_users_ordered[$i]['total'] = (int) $user->total_infected_users ?? 0;
                $i++;
            }
        } 
        
        return [
            'x_axis' => $this->getXAxis('yearly'),
            'infected_users' => $infected_users_ordered ?? [],
            'is_1_month' => $infected_users_count_month == 1 ? true : false,
            'month_year' => $infected_users_count_month == 1 ? config('staticdata.months.' . $temp_month) . ', ' . $year : null
        ];
    }

    private function getInfectedUsersWeekly($year)
    {
        $infected_users = User::select(
                DB::raw('COUNT(id) as total_infected_users'),
                DB::raw('MONTH(declared_covid_positive_at) month'),
                DB::raw('DAY(declared_covid_positive_at) day')
            )
            ->whereNotNull('declared_covid_positive_at')
            ->where('is_covid_positive', true)
            ->where('declared_covid_positive_at', '>=', Carbon::now()->subDays(7))
            ->where('declared_covid_positive_at', '<=', Carbon::now())
            ->groupBy('day')
            ->groupBy('month')
            ->orderBy('month')
            ->get();

        if (!$infected_users->isEmpty()) {
            $i = 0;
            foreach ($infected_users as $user) {
                $infected_users_ordered[$i]['month'] = $user->month - 1;
                $infected_users_ordered[$i]['date'] = $user->day;
                $infected_users_ordered[$i]['total'] = (int) $user->total_infected_users ?? 0;
                $i++;
            }
        }
        
        return [
            'x_axis' => $this->getXAxis('weekly'),
            'infected_users' => $infected_users_ordered ?? [],
        ];
    }

    private function getInfectedUsersHourly($year)
    {
        $infected_users = User::select(
                DB::raw('COUNT(id) as total_infected_users'),
                DB::raw('MONTH(declared_covid_positive_at) month'),
                DB::raw('DAY(declared_covid_positive_at) day'),
                DB::raw('HOUR(declared_covid_positive_at) hour')
            )
            ->whereNotNull('declared_covid_positive_at')
            ->where('is_covid_positive', true)
            ->where('declared_covid_positive_at', '>=', Carbon::now()->subHours(24))
            ->groupBy('day')
            ->groupBy('month')
            ->groupBy('hour')
            ->orderBy('month')
            ->get();
        
        if (!$infected_users->isEmpty()) {
            $i = 0;
            foreach ($infected_users as $user) {
                $infected_users_ordered[$i]['month'] = $user->month - 1;
                $infected_users_ordered[$i]['date'] = $user->day;
                $infected_users_ordered[$i]['hour'] = $user->hour;
                $infected_users_ordered[$i]['total'] = (int) $user->total_infected_users ?? 0;
                $i++;
            }
        }

        return [
            'x_axis' => $this->getXAxis('hourly'),
            'infected_users' => $infected_users_ordered ?? [],
        ];
    }

    private function getXAxis($type)
    {
        switch($type) {
            case 'yearly':
                return 24 * 3600 * 1000 * 31;
                break;
                
            case 'weekly':
                return 24 * 3600 * 1000;
                break;
                
            case 'hourly':
                return 24 * 3600 * 60;
                break;
        }
    }
}
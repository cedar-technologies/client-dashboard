<?php
 
namespace App\Services\Covid19;

use Illuminate\Http\Request;
use App\Models\Covid19\User;
use App\Models\Covid19\Interaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UserService
{ 
    public function datatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        if ($draw == 1) {
            $sort = 'declared_covid_positive_at';
            $order = 'desc';
        } else {
            $sort = config('staticdata.covid19.users.table-header.' . $request['order'][0]['column']);
            $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';
        }

        $total = User::when($request->status != 'all', function($q) use ($request) {
                return $q->where('is_covid_positive', $request->status ? true : false);
            })
            ->when($request->start_date && $request->end_date, function($q) use ($request) {
                $start_date = Carbon::parse($request->start_date)->toDateTimeString();
                $end_date = Carbon::parse($request->end_date)->addDay()->subSecond()->toDateTimeString();

                return $q->where(
                    function($q) use ($start_date, $end_date) {
                        return $q->whereBetween('declared_covid_positive_at', [$start_date, $end_date])
                            ->orWhereNull('declared_covid_positive_at');
                    }
                );
            })
            ->when($request->keyword, function($q) use ($request) {
                return $q->where('mobile_no', 'LIKE', '%'.$request->keyword.'%');
            })->count();

        $user_list = User::select(
            'id',
            'mobile_no',
            'is_covid_positive',
            'is_doctor',
            'is_government',
            'declared_covid_positive_at'
            )->when($request->status != 'all', function($q) use ($request) {
                return $q->where('is_covid_positive', $request->status ? true : false);
            })
            ->when($request->start_date && $request->end_date, function($q) use ($request) {
                $start_date = Carbon::parse($request->start_date)->toDateTimeString();
                $end_date = Carbon::parse($request->end_date)->addDay()->subSecond()->toDateTimeString();

                return $q->where(
                    function($q) use ($start_date, $end_date) {
                        return $q->whereBetween('declared_covid_positive_at', [$start_date, $end_date])
                            ->orWhereNull('declared_covid_positive_at');
                    }
                );
            })
            ->when($request->keyword, function($q) use ($request) {
                return $q->where('mobile_no', 'LIKE', '%'.$request->keyword.'%');
            })
            ->when($sort == 'designation', function($q) use ($order) {
                return $q->orderByRaw('CASE WHEN is_doctor = 1 THEN 1 WHEN is_government = 1 THEN 2 ELSE 3 END ' . $order);
            })
            ->when($sort != 'designation', function($q) use ($sort, $order) {
                return $q->orderBy($sort, $order);
            })
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($user_list as $user) {
            $user->status = $user->is_covid_positive ? 'Positive' : 'Negative';
            if ($user->is_doctor) {
                $user->designation = 'Doctor';
                $designation_value = "1";
            } else if ($user->is_government) {
                $user->designation = 'Government Agent';
                $designation_value = "2";
            } else {
                $user->designation = 'User';
                $designation_value = "3";
            }

            $user->update_btn = '<button class="btn btn-success w-100 btn-update" data-user-id="'.$user->id.'" data-designation="'.$designation_value.'">Update</button>';
            unset($user->is_covid_positive, $user->is_doctor, $user->is_government);
        }

        $user_list = $user_list->toArray();

        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $user_list,
        ];

        return json_encode($data, JSON_PRETTY_PRINT);
    }

    public function getInfectionTreeData(Request $request)
    {
        $data = [
            'name' => 'Patient 0',
            'title' => $request->mobile_no
        ];

        $first_children_layer = rand(2,3);
        $second_children_layer = rand(2,3);
        $total_count = 1;

        for ($i = 1; $i <= $first_children_layer; $i++) {
            $data['children'][$i - 1] = [
                'name' => 'Patient '.$total_count++,
                'title' => '01'.mt_rand(10000000, 99999999)
            ];

            for ($j = 1; $j <= $second_children_layer; $j++) {
                $data['children'][$i - 1]['children'][$j - 1] = [
                    'name' => 'Patient '.$total_count++,
                    'title' => '01'.mt_rand(10000000, 99999999)
                ];

                $have_children = mt_rand(0,1);
                $total_children = mt_rand(2,3);

                if ($have_children) {
                    for ($k = 1; $k <= $total_children; $k++) {
                        $data['children'][$i - 1]['children'][$j - 1]['children'][$k - 1] = [
                            'name' => 'Patient '.$total_count++,
                            'title' => '01'.mt_rand(10000000, 99999999)
                        ];
                    }
                }
            }
        }
        
        return json_encode($data);
    }

    public function update(Request $request, $id)
    {
        if ($request->designation == '1') {
            $update_data = [
                'is_doctor' => true,
                'is_government' => false
            ];
            $return_data = [
                'attributes' => [
                    'designation_id' => 1,
                    'designation_name' => 'Doctor'
                ]
            ];
        } else if ($request->designation == '2') {
            $update_data = [
                'is_doctor' => false,
                'is_government' => true
            ];
            $return_data = [
                'attributes' => [
                    'designation_id' => 2,
                    'designation_name' => 'Government Agent'
                ]
            ];
        } else {
            $update_data = [
                'is_doctor' => false,
                'is_government' => false
            ];
            $return_data = [
                'attributes' => [
                    'designation_id' => 3,
                    'designation_name' => 'User'
                ]
            ];
        }

        User::find($id)->update($update_data);

        return $return_data;
    }

    public function notificationDatatableList(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');

        if ($draw == 1) {
            $sort = 'declared_covid_positive_at';
            $order = 'desc';
        } else {
            $sort = config('staticdata.covid19.notifications.table-header.' . $request['order'][0]['column']);
            $request['order'][0]['dir'] == 'asc' ? $order = 'asc' : $order = 'desc';
        }

        $total = User::where('is_covid_positive', true)
            ->when($request->start_date && $request->end_date, function($q) use ($request) {
                $start_date = Carbon::parse($request->start_date)->toDateTimeString();
                $end_date = Carbon::parse($request->end_date)->addDay()->subSecond()->toDateTimeString();

                return $q->whereBetween('declared_covid_positive_at', [$start_date, $end_date]);
            })
            ->when($request->keyword, function($q) use ($request) {
                return $q->where('mobile_no', 'LIKE', '%'.$request->keyword.'%');
            })->count();

        $user_list = User::select(
            'id',
            'mobile_no',
            'is_covid_positive',
            'is_doctor',
            'is_government',
            'declared_covid_positive_at'
            )->where('is_covid_positive', true)
            ->when($request->start_date && $request->end_date, function($q) use ($request) {
                $start_date = Carbon::parse($request->start_date)->toDateTimeString();
                $end_date = Carbon::parse($request->end_date)->addDay()->subSecond()->toDateTimeString();

                return $q->whereBetween('declared_covid_positive_at', [$start_date, $end_date]);
            })
            ->when($request->keyword, function($q) use ($request) {
                return $q->where('mobile_no', 'LIKE', '%'.$request->keyword.'%');
            })
            ->when($sort == 'designation', function($q) use ($order) {
                return $q->orderByRaw('CASE WHEN is_doctor = 1 THEN 1 WHEN is_government = 1 THEN 2 ELSE 3 END ' . $order);
            })
            ->when($sort != 'designation', function($q) use ($sort, $order) {
                return $q->orderBy($sort, $order);
            })
            ->offset($start)
            ->limit($length)
            ->get();

        foreach ($user_list as $user) {
            $redirect_url_param = [
                'mobile_no' => $user->mobile_no
            ];

            $user->status = $user->is_covid_positive ? 'Positive' : 'Negative';

            if ($user->is_doctor) {
                $user->designation = 'Doctor';
            } else if ($user->is_government) {
                $user->designation = 'Government Agent';
            } else {
                $user->designation = 'User';
            }

            $user->notify_checkbox = '<input type="checkbox" id="checkbox_'.$user->id.'" ' .
                'class="notify-checkbox" data-mobile_no="'.$user->mobile_no.'">' . 
                '<label for="checkbox_'.$user->id.'"></label>';

            $user->mobile_no = '<a href="'.route('covid19.user-interactions.list', $redirect_url_param).'">'.$user->mobile_no.'</a>';

            unset($user->is_covid_positive, $user->is_doctor, $user->is_government);
        }

        $user_list = $user_list->toArray();

        $data = [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $user_list,
        ];

        return json_encode($data, JSON_PRETTY_PRINT);
    }

    public function updateInteractedUserNotifyFlag($request)
    {
        $mobile_no_array = json_decode($request->mobile_no);
        $interacted_with = [];

        foreach ($mobile_no_array as $mobile_no) {
            $user = User::select('declared_covid_positive_at')->whereMobileNo($mobile_no)->first();
            $duration = $this->formatDuration($request->duration, $user->declared_covid_positive_at);
            $interaction_duration = $this->formatInteractionDuration($request->interaction_duration);

            $interactions = Interaction::select(
                'id', 
                'mobile_no_1', 
                'mobile_no_2',
                DB::raw('MIN(ROUND(min_distance, 2)) as min_distance'),
                DB::raw('SUM(TIMESTAMPDIFF(SECOND, interaction_started_at, interaction_ended_at)) as duration')
                )
                ->where(
                    function($q) use ($mobile_no) {
                        return $q->where('mobile_no_1', $mobile_no)
                            ->orWhere('mobile_no_2', $mobile_no);
                    }
                )
                ->when(
                    $duration,
                    function($q) use ($duration) {
                        return $q->where('interaction_started_at', '>=', $duration);
                    }
                )
                ->where('min_distance', '<=', $request->min_distance)
                ->groupBy('mobile_no_1', 'mobile_no_2')
                ->when(
                    $interaction_duration,
                    function($q) use ($interaction_duration) {
                        return $q->having('duration', '>=', $interaction_duration);
                    }
                )
                ->get();
    
            foreach ($interactions as $interaction) {
                if (!in_array($interaction->mobile_no_1, $interacted_with)) {
                    $interacted_with[] = $interaction->mobile_no_1;
                }
    
                if (!in_array($interaction->mobile_no_2, $interacted_with)) {
                    $interacted_with[] = $interaction->mobile_no_2;
                }
            }
        }

        $interacted_with = array_diff($interacted_with, $mobile_no_array);
        User::whereIn('mobile_no', $interacted_with)->update(['notify' => true]);

        return ['message' => 'Success.'];
    }

    private function formatDuration($duration, $from) {
        switch ($duration) {
            case '1':
                return Carbon::parse($from)->subDays(14);
                break;

            case '2':
                return Carbon::parse($from)->subMonth();
                break;

            case '3':
                return null;
                break;
        }
    }

    private function formatInteractionDuration($duration) {
        switch ($duration) {
            case '1':
                return null;
                break;

            case '2':
                return 10;
                break;

            case '3':
                return 60;
                break;

            case '4':
                return 120;
                break;

            case '5':
                return 300;
                break;

            case '6':
                return 600;
                break;

            case '7':
                return 900;
                break;

            case '8':
                return 1800;
                break;

            case '9':
                return 3600;
                break;
        }
    }
}
<?php
namespace App\Services\Uwc;

use App\Models\User;
use App\Models\Uwc\Station1Job;
use App\Models\Uwc\Station2Job;
use App\Models\Uwc\Station3Job;
use Excel;
use Carbon\Carbon;

class ExcelService
{
    public function generateTraceabilityReport($request)
    {
        if ($request->station_no == 1) {
            return $this->generateStation1Report($request);
        } else if ($request->station_no == 2) {
            return $this->generateStation2Report($request);
        } else {
            return $this->generateStation3Report($request);
        }
    }

    private function generateStation1Report($request)
    {
        $data = Station1Job::whereNotNull('batch_end_datetime')
            ->where('production_line_no', $request->production_line_no)
            ->whereBetween('batch_start_datetime', [$request->start_date, $request->end_date])
            ->with('employee')
            ->with(['defects' => function ($q) {
                $q->select('batch_id', 'units', 'reason');
            }])
            ->get();
        
        $i = 5;
        $j = 6;
        $total = count($data) + $j;

        return Excel::create(
            'Prod Line '. $request->production_line_no . ' Station 1 ' . Carbon::parse($request->start_date)->format('Y-m-d') . ' - ' . Carbon::parse($request->end_date)->format('Y-m-d') , 
            function($excel) use ($data, $i, $j, $total, $request) {
                $excel->sheet('Sheet 1', function($sheet) use ($data, $i, $j, $total, $request)
                {
                    $sheet->cells("A$i:AB$total", function($cells) {
                        $cells->setValignment('center');
                        $cells->setAlignment('center');
                    });
                    $sheet->getStyle("A$i:AB$j")->getAlignment()->setWrapText(true);
                    $sheet->setWidth(config('staticdata.excel_header.station1'));
                    $sheet->cell('A1', function($cell) {
                        $cell->setValue('Process Flow Record - Insert Rubber');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->cell('A2', function($cell) {
                        $cell->setValue('Process: Insert Rubber');
                    });
                    $sheet->cell('A3', function($cell) {
                        $cell->setValue('Machine No: ');
                    });
                    $sheet->cell('C1', function($cell) use ($request) {
                        $cell->setValue('Production Line: ' . $request->production_line_no);
                    });
                    $sheet->cell('C2', function($cell) {
                        $cell->setValue('Station: 1');
                    });
                    $sheet->cell('C3', function($cell) use ($request) {
                        $cell->setValue('Date: ' . Carbon::parse($request->start_date)->format('d/m/Y') . ' - ' . Carbon::parse($request->end_date)->format('d/m/Y'));
                    });
                    $sheet->mergeCells("A$i:A$j");
                    $sheet->mergeCells("B$i:C$i");
                    $sheet->mergeCells("D$i:D$j");
                    $sheet->mergeCells("E$i:E$j");
                    $sheet->mergeCells("F$i:F$j");
                    $sheet->mergeCells("G$i:G$j");
                    $sheet->mergeCells("H$i:J$i");
                    $sheet->mergeCells("K$i:W$i");
                    $sheet->mergeCells("X$i:Z$i");
                    $sheet->mergeCells("AA$i:AB$i");
                    $sheet->cell("A$i", function($cell) {
                        $cell->setValue('PFR No');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium'); 
                    });
                    $sheet->cell("B$i", function($cell) {
                        $cell->setValue('Time');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("B$j", function($cell) {
                        $cell->setValue('Start');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("C$j", function($cell) {
                        $cell->setValue('End');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("D$i", function($cell) {
                        $cell->setValue('Performed By');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("E$i", function($cell) {
                        $cell->setValue('Traceability No. (Adaptor Body)');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("F$i", function($cell) {
                        $cell->setValue('Starting Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("G$i", function($cell) {
                        $cell->setValue('Accepted Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("H$i", function($cell) {
                        $cell->setValue('Incomplete Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("H$j", function($cell) {
                        $cell->setValue('End of shift');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("I$j", function($cell) {
                        $cell->setValue('Incomplete Product');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("J$j", function($cell) {
                        $cell->setValue('Other');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("K$i", function($cell) {
                        $cell->setValue('Reject Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("K$j", function($cell) {
                        $cell->setValue('Impurities/Black Dot');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("L$j", function($cell) {
                        $cell->setValue('Dirty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("M$j", function($cell) {
                        $cell->setValue('Short Material');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("N$j", function($cell) {
                        $cell->setValue('Bubbles');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("O$j", function($cell) {
                        $cell->setValue('Thread Damaged');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("P$j", function($cell) {
                        $cell->setValue('Flow Mark');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("Q$j", function($cell) {
                        $cell->setValue('Excessive Material');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("R$j", function($cell) {
                        $cell->setValue('Body Damaged');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("S$j", function($cell) {
                        $cell->setValue('Oil Mark/Oily');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("T$j", function($cell) {
                        $cell->setValue('Rubber Length Out');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("U$j", function($cell) {
                        $cell->setValue('Rubber Location Out');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("V$j", function($cell) {
                        $cell->setValue('QA/Sample');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("W$j", function($cell) {
                        $cell->setValue('Other');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("X$i", function($cell) {
                        $cell->setValue('Sample');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("X$j", function($cell) {
                        $cell->setValue('Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("Y$j", function($cell) {
                        $cell->setValue('To (location)');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("Z$j", function($cell) {
                        $cell->setValue('Trans. No');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("AA$i", function($cell) {
                        $cell->setValue('Review By');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("AA$j", function($cell) {
                        $cell->setValue('Date');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("AB$j", function($cell) {
                        $cell->setValue('Name');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });

                    if (!empty($data)) {
                        foreach ($data as $key => $value) {
                            $x= $key+$j+1;
                            $sheet->cell('A'.$x, $value->pfr_1_no); 
                            $sheet->cell('B'.$x, Carbon::parse($value->batch_start_datetime)->format('d/m/Y g:ia')); 
                            $sheet->cell('C'.$x, Carbon::parse($value->batch_end_datetime)->format('d/m/Y g:ia')); 
                            $sheet->cell('D'.$x, $value->employee_id . ' (' . $value->employee->name . ')'); 
                            $sheet->cell('E'.$x, $value->body_traceability_no); 
                            $sheet->cell('F'.$x, $value->completed + $value->incomplete); 
                            $sheet->cell('G'.$x, $value->completed);

                            if ($value->reason_of_incompletion == 'End of shift') {
                                $sheet->cell('H'.$x, $value->incomplete);
                            } else if ($value->reason_of_incompletion == 'Incomplete Product') {
                                $sheet->cell('I'.$x, $value->incomplete);
                            } else {
                                $sheet->cell('J'.$x, $value->incomplete);
                            }

                            foreach ($value->defects as $defect) {
                                switch ($defect->reason) {
                                    case 'Impurities/Black Dot':
                                        $sheet->cell('K'.$x, $defect->units);
                                        break;
                                    case 'Dirty':
                                        $sheet->cell('L'.$x, $defect->units);
                                        break;
                                    case 'Short Material':
                                        $sheet->cell('M'.$x, $defect->units);
                                        break;
                                    case 'Bubbles':
                                        $sheet->cell('N'.$x, $defect->units);
                                        break;
                                    case 'Thread Damaged':
                                        $sheet->cell('O'.$x, $defect->units);
                                        break;
                                    case 'Flow Mark':
                                        $sheet->cell('P'.$x, $defect->units);
                                        break;
                                    case 'Excessive Material':
                                        $sheet->cell('Q'.$x, $defect->units);
                                        break;
                                    case 'Body Damaged':
                                        $sheet->cell('R'.$x, $defect->units);
                                        break;
                                    case 'Oil Mark/Oily':
                                        $sheet->cell('S'.$x, $defect->units);
                                        break;
                                    case 'Rubber Length Out':
                                        $sheet->cell('T'.$x, $defect->units);
                                        break;
                                    case 'Rubber Location Out':
                                        $sheet->cell('U'.$x, $defect->units);
                                        break;
                                    case 'QA/Sample':
                                        $sheet->cell('V'.$x, $defect->units);
                                        break;
                                    case 'Other':
                                        $sheet->cell('W'.$x, $defect->units);
                                        break;
                                }
                            }
                        }
                    }
                });
            })->download('xlsx');
    }

    private function generateStation2Report($request)
    {
        $data = Station2Job::whereNotNull('batch_end_datetime')
            ->where('production_line_no', $request->production_line_no)
            ->whereBetween('batch_start_datetime', [$request->start_date, $request->end_date])
            ->with('employee')
            ->with(['defects' => function ($q) {
                $q->select('batch_id', 'units', 'reason');
            }])
            ->get();

        $i = 5;
        $j = 6;
        $total = count($data) + $j; 

        return Excel::create(
            'Prod Line '. $request->production_line_no . ' Station 2 ' . Carbon::parse($request->start_date)->format('Y-m-d') . ' - ' . Carbon::parse($request->end_date)->format('Y-m-d'), 
            function($excel) use ($data, $i, $j, $total, $request) {
                $excel->sheet('Sheet 1', function($sheet) use ($data, $i, $j, $total, $request)
                {
                    $sheet->cells("A$i:Y$total", function($cells) {
                        $cells->setValignment('center');
                        $cells->setAlignment('center');
                    });
                    $sheet->getStyle("A$i:Y$j")->getAlignment()->setWrapText(true);
                    $sheet->setWidth(config('staticdata.excel_header.station2'));
                    $sheet->cell('A1', function($cell) {
                        $cell->setValue('Process Flow Record - Insert Cap');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->cell('A2', function($cell) {
                        $cell->setValue('Process: Insert Cap');
                    });
                    $sheet->cell('A3', function($cell) {
                        $cell->setValue('Machine No: ');
                    });
                    $sheet->cell('C1', function($cell) use ($request) {
                        $cell->setValue('Production Line: ' . $request->production_line_no);
                    });
                    $sheet->cell('C2', function($cell) {
                        $cell->setValue('Station: 2');
                    });
                    $sheet->cell('C3', function($cell) use ($request) {
                        $cell->setValue('Date: ' . Carbon::parse($request->start_date)->format('d/m/Y') . ' - ' . Carbon::parse($request->end_date)->format('d/m/Y'));
                    });
                    $sheet->mergeCells("A$i:A$j");
                    $sheet->mergeCells("B$i:C$i");
                    $sheet->mergeCells("D$i:D$j");
                    $sheet->mergeCells("E$i:E$j");
                    $sheet->mergeCells("F$i:F$j");
                    $sheet->mergeCells("G$i:G$j");
                    $sheet->mergeCells("H$i:H$j");
                    $sheet->mergeCells("I$i:K$i");
                    $sheet->mergeCells("L$i:T$i");
                    $sheet->mergeCells("U$i:W$i");
                    $sheet->mergeCells("X$i:Y$i");
                    $sheet->cell("A$i", function($cell) {
                        $cell->setValue('PFR No');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium'); 
                    });
                    $sheet->cell("B$i", function($cell) {
                        $cell->setValue('Time');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("B$j", function($cell) {
                        $cell->setValue('Start');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("C$j", function($cell) {
                        $cell->setValue('End');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("D$i", function($cell) {
                        $cell->setValue('Performed By');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("E$i", function($cell) {
                        $cell->setValue('Traceability No. (Adaptor Body)');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("F$i", function($cell) {
                        $cell->setValue('Previous Process PFR Insert Rubber No.');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("G$i", function($cell) {
                        $cell->setValue('Starting Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("H$i", function($cell) {
                        $cell->setValue('Accepted Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("I$i", function($cell) {
                        $cell->setValue('Incomplete Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("I$j", function($cell) {
                        $cell->setValue('End of shift');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("J$j", function($cell) {
                        $cell->setValue('Incomplete Product');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("K$j", function($cell) {
                        $cell->setValue('Other');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("L$i", function($cell) {
                        $cell->setValue('Reject Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("L$j", function($cell) {
                        $cell->setValue('Impurities/Black Dot');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("M$j", function($cell) {
                        $cell->setValue('Dirty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("N$j", function($cell) {
                        $cell->setValue('Cap Damaged');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("O$j", function($cell) {
                        $cell->setValue('Body Damaged');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("P$j", function($cell) {
                        $cell->setValue('Oil Mark/Oily');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("Q$j", function($cell) {
                        $cell->setValue('Overpress');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("R$j", function($cell) {
                        $cell->setValue('Press Incomplete');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("S$j", function($cell) {
                        $cell->setValue('QA/Sample');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("T$j", function($cell) {
                        $cell->setValue('Other');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("U$i", function($cell) {
                        $cell->setValue('Sample');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("U$j", function($cell) {
                        $cell->setValue('Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("V$j", function($cell) {
                        $cell->setValue('To (location)');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("W$j", function($cell) {
                        $cell->setValue('Trans. No');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("X$i", function($cell) {
                        $cell->setValue('Review By');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("X$j", function($cell) {
                        $cell->setValue('Date');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("Y$j", function($cell) {
                        $cell->setValue('Name');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });

                    if (!empty($data)) {
                        foreach ($data as $key => $value) {
                            $x= $key+$j+1;
                            $sheet->cell('A'.$x, $value->pfr_2_no); 
                            $sheet->cell('B'.$x, Carbon::parse($value->batch_start_datetime)->format('d/m/Y g:ia')); 
                            $sheet->cell('C'.$x, Carbon::parse($value->batch_end_datetime)->format('d/m/Y g:ia')); 
                            $sheet->cell('D'.$x, $value->employee_id . ' (' . $value->employee->name . ')'); 
                            $sheet->cell('E'.$x, $value->body_traceability_no); 
                            $sheet->cell('F'.$x, $value->pfr_1_no); 
                            $sheet->cell('G'.$x, $value->completed + $value->incomplete); 
                            $sheet->cell('H'.$x, $value->completed);

                            if ($value->reason_of_incompletion == 'End of shift') {
                                $sheet->cell('I'.$x, $value->incomplete);
                            } else if ($value->reason_of_incompletion == 'Incomplete Product') {
                                $sheet->cell('J'.$x, $value->incomplete);
                            } else {
                                $sheet->cell('K'.$x, $value->incomplete);
                            }

                            foreach ($value->defects as $defect) {
                                switch ($defect->reason) {
                                    case 'Impurities/Black Dot':
                                        $sheet->cell('L'.$x, $defect->units);
                                        break;
                                    case 'Dirty':
                                        $sheet->cell('M'.$x, $defect->units);
                                        break;
                                    case 'Cap Damaged':
                                        $sheet->cell('N'.$x, $defect->units);
                                        break;
                                    case 'Body Damaged':
                                        $sheet->cell('O'.$x, $defect->units);
                                        break;
                                    case 'Oil Mark/Oily':
                                        $sheet->cell('P'.$x, $defect->units);
                                        break;
                                    case 'Overpress':
                                        $sheet->cell('Q'.$x, $defect->units);
                                        break;
                                    case 'Press Incomplete':
                                        $sheet->cell('R'.$x, $defect->units);
                                        break;
                                    case 'QA/Sample':
                                        $sheet->cell('S'.$x, $defect->units);
                                        break;
                                    case 'Other':
                                        $sheet->cell('T'.$x, $defect->units);
                                        break;
                                }
                            }
                        }
                    }
                });
            })->download('xlsx');
    }

    private function generateStation3Report($request)
    {
        $data = Station3Job::whereNotNull('batch_end_datetime')
            ->where('production_line_no', $request->production_line_no)
            ->whereBetween('batch_start_datetime', [$request->start_date, $request->end_date])
            ->with('employee')
            ->with(['defects' => function ($q) {
                $q->select('batch_id', 'units', 'reason');
            }])
            ->get();

        $i = 5;
        $j = 6;
        $total = count($data) + $j; 

        return Excel::create(
            'Prod Line '. $request->production_line_no . ' Station 3 ' . Carbon::parse($request->start_date)->format('Y-m-d') . ' - ' . Carbon::parse($request->end_date)->format('Y-m-d'), 
            function($excel) use ($data, $i, $j, $total, $request) {
                $excel->sheet('Sheet 1', function($sheet) use ($data, $i, $j, $total, $request)
                {
                    $sheet->cells("A$i:Y$total", function($cells) {
                        $cells->setValignment('center');
                        $cells->setAlignment('center');
                    });
                    $sheet->getStyle("A$i:Y$j")->getAlignment()->setWrapText(true);
                    $sheet->setWidth(config('staticdata.excel_header.station3'));
                    $sheet->cell('A1', function($cell) {
                        $cell->setValue('Process Flow Record - Packing');
                        $cell->setFontWeight('bold');
                    });
                    $sheet->cell('A2', function($cell) {
                        $cell->setValue('Process: Packing');
                    });
                    $sheet->cell('A3', function($cell) {
                        $cell->setValue('Machine No: ');
                    });
                    $sheet->cell('C1', function($cell) use ($request) {
                        $cell->setValue('Production Line: ' . $request->production_line_no);
                    });
                    $sheet->cell('C2', function($cell) {
                        $cell->setValue('Station: 3');
                    });
                    $sheet->cell('C3', function($cell) use ($request) {
                        $cell->setValue('Date: ' . Carbon::parse($request->start_date)->format('d/m/Y') . ' - ' . Carbon::parse($request->end_date)->format('d/m/Y'));
                    });
                    $sheet->mergeCells("A$i:A$j");
                    $sheet->mergeCells("B$i:C$i");
                    $sheet->mergeCells("D$i:D$j");
                    $sheet->mergeCells("E$i:E$j");
                    $sheet->mergeCells("F$i:F$j");
                    $sheet->mergeCells("G$i:G$j");
                    $sheet->mergeCells("H$i:H$j");
                    $sheet->mergeCells("I$i:I$j");
                    $sheet->mergeCells("J$i:L$i");
                    $sheet->mergeCells("M$i:S$i");
                    $sheet->mergeCells("T$i:T$j");
                    $sheet->mergeCells("U$i:W$i");
                    $sheet->mergeCells("X$i:Y$i");
                    $sheet->cell("A$i", function($cell) {
                        $cell->setValue('PFR No');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium'); 
                    });
                    $sheet->cell("B$i", function($cell) {
                        $cell->setValue('Time');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("B$j", function($cell) {
                        $cell->setValue('Start');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("C$j", function($cell) {
                        $cell->setValue('End');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("D$i", function($cell) {
                        $cell->setValue('Performed By');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("E$i", function($cell) {
                        $cell->setValue('Traceability No. (Pouch)');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("F$i", function($cell) {
                        $cell->setValue('Traceability No. (Body Tube)');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("G$i", function($cell) {
                        $cell->setValue('Previous Process PFR Insert Cap No.');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("H$i", function($cell) {
                        $cell->setValue('Starting Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("I$i", function($cell) {
                        $cell->setValue('Accepted Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("J$i", function($cell) {
                        $cell->setValue('Incomplete Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("J$j", function($cell) {
                        $cell->setValue('End of shift');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("K$j", function($cell) {
                        $cell->setValue('Incomplete Product');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("L$j", function($cell) {
                        $cell->setValue('Other');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("M$i", function($cell) {
                        $cell->setValue('Reject Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("M$j", function($cell) {
                        $cell->setValue('Impurities/Black Dot');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("N$j", function($cell) {
                        $cell->setValue('Seal Incomplete');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("O$j", function($cell) {
                        $cell->setValue('Seal Damaged');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("P$j", function($cell) {
                        $cell->setValue('Pouch Printing Problem');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("Q$j", function($cell) {
                        $cell->setValue('Lot Printing Problem');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("R$j", function($cell) {
                        $cell->setValue('QA/Sample');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("S$j", function($cell) {
                        $cell->setValue('Other');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("T$i", function($cell) {
                        $cell->setValue('Use for Lot');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("U$i", function($cell) {
                        $cell->setValue('Sample');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("U$j", function($cell) {
                        $cell->setValue('Qty');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("V$j", function($cell) {
                        $cell->setValue('To (location)');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("W$j", function($cell) {
                        $cell->setValue('Trans. No');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("X$i", function($cell) {
                        $cell->setValue('Review By');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("X$j", function($cell) {
                        $cell->setValue('Date');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });
                    $sheet->cell("Y$j", function($cell) {
                        $cell->setValue('Name');
                        $cell->setBorder('medium', 'medium', 'medium', 'medium');
                    });

                    if (!empty($data)) {
                        foreach ($data as $key => $value) {
                            $x= $key+$j+1;
                            $sheet->cell('A'.$x, $value->pfr_2_no); 
                            $sheet->cell('B'.$x, Carbon::parse($value->batch_start_datetime)->format('d/m/Y g:ia')); 
                            $sheet->cell('C'.$x, Carbon::parse($value->batch_end_datetime)->format('d/m/Y g:ia')); 
                            $sheet->cell('D'.$x, $value->employee_id . ' (' . $value->employee->name . ')'); 
                            $sheet->cell('E'.$x, $value->body_traceability_no); 
                            $sheet->cell('F'.$x, $value->pfr_1_no); 
                            $sheet->cell('G'.$x, $value->completed + $value->incomplete); 
                            $sheet->cell('H'.$x, $value->completed);

                            if ($value->reason_of_incompletion == 'End of shift') {
                                $sheet->cell('I'.$x, $value->incomplete);
                            } else if ($value->reason_of_incompletion == 'Incomplete Product') {
                                $sheet->cell('J'.$x, $value->incomplete);
                            } else {
                                $sheet->cell('K'.$x, $value->incomplete);
                            }

                            foreach ($value->defects as $defect) {
                                switch ($defect->reason) {
                                    case 'Impurities/Black Dot':
                                        $sheet->cell('M'.$x, $defect->units);
                                        break;
                                    case 'Seal Incomplete':
                                        $sheet->cell('N'.$x, $defect->units);
                                        break;
                                    case 'Seal Damaged':
                                        $sheet->cell('O'.$x, $defect->units);
                                        break;
                                    case 'Pouch Printing Problem':
                                        $sheet->cell('P'.$x, $defect->units);
                                        break;
                                    case 'Lot Printing Problem':
                                        $sheet->cell('Q'.$x, $defect->units);
                                        break;
                                    case 'QA/Sample':
                                        $sheet->cell('R'.$x, $defect->units);
                                        break;
                                    case 'Other':
                                        $sheet->cell('S'.$x, $defect->units);
                                        break;
                                }
                            }
                        }
                    }
                });
            })->download('xlsx');
    }
}
?>
<?php

namespace App\Services\Uwc;

use Illuminate\Http\Request;
use App\Models\Uwc\Employee;
use App\Models\Uwc\EmployeeShift;

class EmployeeService
{
    public function getTotalWorkingHours($employee_id)
    {
        $sum = EmployeeShift::where('employee_id', $employee_id)
            ->where('clock_out_datetime', '!=', 'NULL')
            ->sum('working_hours');
        
        return $sum;
    }
    
    public function formatInHours($value)
    {
        // Working hours in minutes, divided by 60 to make hours
        $total = $value / 60;

        return number_format($total, 2, '.', ',');
    }
}
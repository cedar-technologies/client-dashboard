<?php

namespace App\Services\Uwc;

use Illuminate\Http\Request;
use App\Models\Uwc\EmployeeShift;
use App\Models\Uwc\Defect;
use App\Models\Uwc\Station1Job;
use App\Models\Uwc\Station2Job;
use App\Models\Uwc\Station3Job;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AnalyticsService
{
    public function getDailyPercentage($production_line = null)
    {
        return [
            'working_hours' => $this->getDailyAverageWorkingHours($production_line),
            'total_units' => $this->getDailyTotalUnits($production_line),
            'total_avg_units_per_hour' => $this->getDailyAverageUnitsCompletedPerHour($production_line),
            'defects_percentage' => $this->getDailyAverageDefectivePercentage($production_line),
        ];
    }

    public function getDailyAverageWorkingHours($production_line = null)
    {
        $sum = EmployeeShift::
            when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('clock_out_datetime', '!=', 'NULL')
            ->whereBetween('clock_out_datetime', [Carbon::now()->subHours(24), Carbon::now()])
            ->sum('working_hours');

        $count = EmployeeShift::
            when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('clock_out_datetime', '!=', 'NULL')
            ->whereBetween('clock_out_datetime', [Carbon::now()->subHours(24), Carbon::now()])
            ->count();

        if ($sum > 0 && $count > 0) {
            $total = $sum / 60 / $count;
        } else {
            $total = 0;
        }

        if ($total > 12) {
            $total = 12;
        }

        return (float) $total;
    }

    public function getDailyTotalUnits($production_line = null)
    {
        $station1 = Station1Job::
            when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->whereBetween('batch_end_datetime', [Carbon::now()->subHours(24), Carbon::now()])
            ->sum('completed');

        $station2 = Station2Job::
            when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->whereBetween('batch_end_datetime', [Carbon::now()->subHours(24), Carbon::now()])
            ->sum('completed');
            
        $station3 = Station3Job::
            when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->whereBetween('batch_end_datetime', [Carbon::now()->subHours(24), Carbon::now()])
            ->sum('completed');
        
        $total = $station1 + $station2 + $station3;
        return (int) $total;
    }
  
    public function getDailyAverageUnitsCompletedPerHour($production_line = null)
    {
        $total = $this->getDailyTotalUnits($production_line);

        $total = $total / 24; // total divided by 24 hours

        return (float) $total;
    }
    
    public function getDailyAverageDefectivePercentage($production_line = null)
    {
        $defects = Defect::
            when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->whereBetween('created_at', [Carbon::now()->subHours(24), Carbon::now()])
            ->sum('units');
        
        $total_units = $this->getDailyTotalUnits($production_line);

        if ($total_units > 0) {
            $percentage = $defects / ($total_units + $defects) * 100;
        } else {
            $percentage = 0;
        }

        return $percentage;
    }
    
    public function getUnitsCompleted($production_line = null, $filter = 'yearly')
    {
        $year = Carbon::now()->year;

        switch($filter) {
            case 'yearly':
                $data = $this->getUnitsCompletedYearly($production_line, $year);
                $title = "Units completion in $year";
                $subtitle = "Number of units completed each month in current year";
                break;

            case 'weekly':
                $data = $this->getUnitsCompletedWeekly($production_line, $year);
                $title = "Units completion in past 1 week";
                $subtitle = "Number of units completed each day in past 1 week";
                break;

            case 'hourly':
                $data = $this->getUnitsCompletedHourly($production_line, $year);
                $title = "Units completion in past 24 hours";
                $subtitle = "Number of units completed each hour in past 24 hours";
                break;
        }

        return [
            'year' => $year,
            'title' => $title,
            'subtitle' => $subtitle,
            'x_axis' => $data['x_axis'],
            'station1' => $data['station1'],
            'station2' => $data['station2'],
            'station3' => $data['station3'],
            'is_1_month' => $data['is_1_month'] ?? false,
            'month_year' => $data['month_year'] ?? null
        ];
    }

    public function getDefectiveUnits($production_line = null, $filter = 'yearly')
    {
        $year = Carbon::now()->year;

        switch($filter) {
            case 'yearly':
                $data = $this->getDefectiveUnitsYearly($production_line, $year);
                $title = "Defective Units in $year";
                $subtitle = "Number of defective units each month in current year";
                break;

            case 'weekly':
                $data = $this->getDefectiveUnitsWeekly($production_line, $year);
                $title = "Defective Units in past 1 week";
                $subtitle = "Number of defective units each days in past 1 week";
                break;

            case 'hourly':
                $data = $this->getDefectiveUnitsHourly($production_line, $year);
                $title = "Defective Units in past 24 hours";
                $subtitle = "Number of defective units each hour in past 24 hours";
                break;
        }

        return [
            'year' => $year,
            'title' => $title,
            'subtitle' => $subtitle,
            'x_axis' => $data['x_axis'],
            'defects' => $data['defects'],
            'is_1_month' => $data['is_1_month'] ?? false,
            'month_year' => $data['month_year'] ?? null
        ];
    }

    private function getUnitsCompletedYearly($production_line = null, $year)
    {
        $station1 = Station1Job::select(
                DB::raw('SUM(completed) as completed'),
                DB::raw('MONTH(batch_start_datetime) month')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->whereYear('batch_start_datetime', $year)
            ->groupBy('month')
            ->get();
        
        $station2 = Station2Job::select(
                DB::raw('SUM(completed) as completed'),
                DB::raw('MONTH(batch_start_datetime) month')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->whereYear('batch_start_datetime', $year)
            ->groupBy('month')
            ->get();
        
        $station3 = Station3Job::select(
                DB::raw('SUM(completed) as completed'),
                DB::raw('MONTH(batch_start_datetime) month')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->whereYear('batch_start_datetime', $year)
            ->groupBy('month')
            ->get();
        
        $station_count_month = 1;
        $temp_month = 0;

        if (!$station1->isEmpty()) {
            $i = 0;
            foreach ($station1 as $job) {
                if ($i != 0 && $temp_month != $job->month - 1) {
                    $station_count_month++;
                    $temp_month = $job->month - 1;
                }
                if ($i == 0) {
                    $temp_month = $job->month - 1;
                }
                $station1_ordered[$i]['month'] = $job->month - 1;
                $station1_ordered[$i]['units'] = (int) $job->completed ?? 0;
                $i++;
            }
        } 
        
        if (!$station2->isEmpty()) {
            $i = 0;
            foreach ($station2 as $job) {
                $station2_ordered[$i]['month'] = $job->month - 1;
                $station2_ordered[$i]['units'] = (int) $job->completed ?? 0;
                $i++;
            }
        } 
        
        if (!$station3->isEmpty()) {
            $i = 0;
            foreach ($station3 as $job) {
                $station3_ordered[$i]['month'] = $job->month - 1;
                $station3_ordered[$i]['units'] = (int) $job->completed ?? 0;
                $i++;
            }
        }

        return [
            'x_axis' => $this->getXAxis('yearly'),
            'station1' => $station1_ordered ?? [],
            'station2' => $station2_ordered ?? [],
            'station3' => $station3_ordered ?? [],
            'is_1_month' => $station_count_month == 1 ? true : false,
            'month_year' => $station_count_month == 1 ? config('staticdata.months.' . $temp_month) . ', ' . $year : null
        ];
    }

    private function getUnitsCompletedWeekly($production_line = null, $year)
    {
        $station1 = Station1Job::select(
                DB::raw('SUM(completed) completed'),
                DB::raw('MONTH(batch_start_datetime) month'),
                DB::raw('DAY(batch_start_datetime) day')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('batch_start_datetime', '>=', Carbon::now()->subDays(7))
            ->where('batch_start_datetime', '<=', Carbon::now())
            ->groupBy('day')
            ->groupBy('month')
            ->orderBy('month')
            ->get();

        $station2 = Station2Job::select(
                DB::raw('SUM(completed) completed'),
                DB::raw('MONTH(batch_start_datetime) month'),
                DB::raw('DAY(batch_start_datetime) day')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('batch_start_datetime', '>=', Carbon::now()->subDays(7))
            ->groupBy('day')
            ->groupBy('month')
            ->orderBy('month')
            ->get();
            
        
        $station3 = Station3Job::select(
                DB::raw('SUM(completed) completed'),
                DB::raw('MONTH(batch_start_datetime) month'),
                DB::raw('DAY(batch_start_datetime) day')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('batch_start_datetime', '>=', Carbon::now()->subDays(7))
            ->groupBy('day')
            ->groupBy('month')
            ->orderBy('month')
            ->get();
            
        
        if (!$station1->isEmpty()) {
            $i = 0;
            foreach ($station1 as $job) {
                $station1_ordered[$i]['month'] = $job->month - 1;
                $station1_ordered[$i]['date'] = $job->day;
                $station1_ordered[$i]['units'] = (int) $job->completed ?? 0;
                $i++;
            }
        }
        
        if (!$station2->isEmpty()) {
            $i = 0;
            foreach ($station2 as $job) {
                $station2_ordered[$i]['month'] = $job->month - 1;
                $station2_ordered[$i]['date'] = $job->day;
                $station2_ordered[$i]['units'] = (int) $job->completed ?? 0;
                $i++;
            }
        } 
        
        if (!$station3->isEmpty()) {
            $i = 0;
            foreach ($station3 as $job) {
                $station3_ordered[$i]['month'] = $job->month - 1;
                $station3_ordered[$i]['date'] = $job->day;
                $station3_ordered[$i]['units'] = (int) $job->completed ?? 0;
                $i++;
            }
        }

        return [
            'x_axis' => $this->getXAxis('weekly'),
            'station1' => $station1_ordered ?? [],
            'station2' => $station2_ordered ?? [],
            'station3' => $station3_ordered ?? []
        ];
    }

    private function getUnitsCompletedHourly($production_line = null, $year)
    {
        $station1 = Station1Job::select(
                DB::raw('SUM(completed) completed'),
                DB::raw('MONTH(batch_start_datetime) month'),
                DB::raw('DAY(batch_start_datetime) day'),
                DB::raw('HOUR(batch_start_datetime) hour')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('batch_start_datetime', '>=', Carbon::now()->subHours(24))
            ->groupBy('day')
            ->groupBy('month')
            ->groupBy('hour')
            ->orderBy('month')
            ->get();
        
        $station2 = Station2Job::select(
                DB::raw('SUM(completed) completed'),
                DB::raw('MONTH(batch_start_datetime) month'),
                DB::raw('DAY(batch_start_datetime) day'),
                DB::raw('HOUR(batch_start_datetime) hour')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('batch_start_datetime', '>=', Carbon::now()->subHours(24))
            ->groupBy('day')
            ->groupBy('month')
            ->groupBy('hour')
            ->orderBy('month')
            ->get();
            
        
        $station3 = Station3Job::select(
                DB::raw('SUM(completed) completed'),
                DB::raw('MONTH(batch_start_datetime) month'),
                DB::raw('DAY(batch_start_datetime) day'),
                DB::raw('HOUR(batch_start_datetime) hour')
            )
            ->when(
                $production_line,
                function ($q) use ($production_line) {
                    return $q->where('production_line_no', $production_line);
                }
            )
            ->where('batch_start_datetime', '>=', Carbon::now()->subHours(24))
            ->groupBy('day')
            ->groupBy('month')
            ->groupBy('hour')
            ->orderBy('month')
            ->get();
            
        if (!$station1->isEmpty()) {
            $i = 0;
            foreach ($station1 as $job) {
                $station1_ordered[$i]['month'] = $job->month - 1;
                $station1_ordered[$i]['date'] = $job->day;
                $station1_ordered[$i]['hour'] = $job->hour;
                $station1_ordered[$i]['units'] = (int) $job->completed ?? 0;
                $i++;
            }
        }
        
        if (!$station2->isEmpty()) {
            $i = 0;
            foreach ($station2 as $job) {
                $station2_ordered[$i]['month'] = $job->month - 1;
                $station2_ordered[$i]['date'] = $job->day;
                $station2_ordered[$i]['hour'] = $job->hour;
                $station2_ordered[$i]['units'] = (int) $job->completed ?? 0;
                $i++;
            }
        } 
        
        if (!$station3->isEmpty()) {
            $i = 0;
            foreach ($station3 as $job) {
                $station3_ordered[$i]['month'] = $job->month - 1;
                $station3_ordered[$i]['date'] = $job->day;
                $station3_ordered[$i]['hour'] = $job->hour;
                $station3_ordered[$i]['units'] = (int) $job->completed ?? 0;
                $i++;
            }
        }

        return [
            'x_axis' => $this->getXAxis('hourly'),
            'station1' => $station1_ordered ?? [],
            'station2' => $station2_ordered ?? [],
            'station3' => $station3_ordered ?? []
        ];
    }

    private function getDefectiveUnitsYearly($production_line = null, $year)
    {
        $defects = Defect::select(
            'object_type',
            DB::raw('MONTH(created_at) month'),
            DB::raw('COUNT(id) as defect')
        )
        ->when(
            $production_line,
            function ($q) use ($production_line) {
                return $q->where('production_line_no', $production_line);
            }
        )
        ->whereYear('created_at', $year)
        ->groupBy('object_type','month')
        ->get();

        $station_count_month = 1;
        $temp_month = 0;

        if ($defects) {
            $i = 0;
            $x = 0;
            $temp_object_type = 'Body';
            foreach ($defects as $defect) {
                if ($temp_object_type != $defect->object_type) {
                    $i = 0;
                }
                if ($x != 0 && $temp_month != $defect->month - 1) {
                    $station_count_month++;
                    $temp_month = $defect->month - 1;
                }
                if ($x == 0) {
                    $temp_month = $defect->month - 1;
                }
                $defects_ordered[ucfirst($defect->object_type)][$i]['month'] = $defect->month - 1;
                $defects_ordered[ucfirst($defect->object_type)][$i]['units'] = (int) $defect->defect ?? 0;
                $i++;
                $x++;
                $temp_object_type = $defect->object_type;
            }
        }
        
        foreach (config('staticdata.defects.object_type') as $object) {
            $result['defects'][$object] = $defects_ordered[$object] ?? [];
        }

        $result['x_axis'] = $this->getXAxis('yearly');
        $result['is_1_month'] = $station_count_month == 1 ? true : false;
        $result['month_year'] = $station_count_month == 1 ? config('staticdata.months.' . $temp_month) . ', ' . $year : null;

        return $result;
    }

    private function getDefectiveUnitsWeekly($production_line = null, $year)
    {
        $defects = Defect::select(
            'object_type',
            DB::raw('MONTH(created_at) month'),
            DB::raw('DAY(created_at) day'),
            DB::raw('COUNT(id) as defect')
        )
        ->when(
            $production_line,
            function ($q) use ($production_line) {
                return $q->where('production_line_no', $production_line);
            }
        )
        ->where('created_at', '>=', Carbon::now()->subDays(7))
        ->where('created_at', '<=', Carbon::now())
        ->groupBy('object_type', 'day', 'month')
        ->orderBy('month')
        ->get();

        if ($defects) {
            $i = 0;
            $temp_object_type = 'Body';
            foreach ($defects as $defect) {
                if ($temp_object_type != $defect->object_type) {
                    $i = 0;
                }
                $defects_ordered[ucfirst($defect->object_type)][$i]['month'] = $defect->month - 1;
                $defects_ordered[ucfirst($defect->object_type)][$i]['date'] = $defect->day;
                $defects_ordered[ucfirst($defect->object_type)][$i]['units'] = (int) $defect->defect ?? 0;
                $i++;
                $temp_object_type = $defect->object_type;
            }
        }
        
        foreach (config('staticdata.defects.object_type') as $object) {
            $result['defects'][$object] = $defects_ordered[$object] ?? [];
        }

        $result['x_axis'] = $this->getXAxis('weekly');

        return $result;
    }

    private function getDefectiveUnitsHourly($production_line = null, $year)
    {
        $defects = Defect::select(
            'object_type',
            DB::raw('MONTH(created_at) month'),
            DB::raw('DAY(created_at) day'),
            DB::raw('HOUR(created_at) hour'),
            DB::raw('COUNT(id) as defect')
        )
        ->when(
            $production_line,
            function ($q) use ($production_line) {
                return $q->where('production_line_no', $production_line);
            }
        )
        ->where('created_at', '>=', Carbon::now()->subHours(24))
        ->groupBy('object_type', 'day', 'month', 'hour')
        ->orderBy('month')
        ->get();

        if ($defects) {
            $i = 0;
            $temp_object_type = 'Body';
            foreach ($defects as $defect) {
                if ($temp_object_type != $defect->object_type) {
                    $i = 0;
                }
                $defects_ordered[ucfirst($defect->object_type)][$i]['month'] = $defect->month - 1;
                $defects_ordered[ucfirst($defect->object_type)][$i]['date'] = $defect->day;
                $defects_ordered[ucfirst($defect->object_type)][$i]['hour'] = $defect->hour;
                $defects_ordered[ucfirst($defect->object_type)][$i]['units'] = (int) $defect->defect ?? 0;
                $i++;
                $temp_object_type = $defect->object_type;
            }
        }
        
        foreach (config('staticdata.defects.object_type') as $object) {
            $result['defects'][$object] = $defects_ordered[$object] ?? [];
        }

        $result['x_axis'] = $this->getXAxis('hourly');

        return $result;
    }

    private function getXAxis($type)
    {
        switch($type) {
            case 'yearly':
                return 24 * 3600 * 1000 * 31;
                break;
                
            case 'weekly':
                return 24 * 3600 * 1000;
                break;
                
            case 'hourly':
                return 24 * 3600 * 60;
                break;
        }
    }
}

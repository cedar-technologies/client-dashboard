@extends('covid19.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('covid19.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </div>
            </div>

            {{-- Search and Filter --}}
            <div class="row">
                <div class="col-md-2 col-6">
                    <label for="start_date_range">Date From:</label>
                    <div class="form-group">
                        <input name="start_date_range" id="start_date_range">
                    </div>
                </div>
                <div class="col-md-2 col-6">
                    <label for="end_date_range">Date To:</label>
                    <div class="form-group">
                        <input name="end_date_range" id="end_date_range">
                    </div>
                </div>
                <div class="col-md-2 col-6">
                    <label for="status_dropdown">Status:</label>
                    <div class="form-group">
                        <select class="custom-select select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                            name="status_dropdown" id="status_dropdown" style="width: 100% !important;">
                            <option value="all">All</option>
                            <option value="1">Positive</option>
                            <option value="0">Negative</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-6">
                    <label for="keyword">Search:</label>
                    <div class="input-group">
                        <input type="text" id="keyword" class="form-control" placeholder="Search mobile number.."
                            aria-label="Search mobile number.." aria-describedby="basic-addon2">

                        <div class="input-group-append">
                            <button class="btn btn-success" id="search_btn" type="button"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-4 col-12">
                    <div class="web-view mt-md-4 pt-md-2 mt-3 pt-2"></div>
                    <div class="form-group">
                        <button class="w-100 btn btn-success" id="reset_filter_btn" type="button"><i class="fas fa-undo pr-2"></i>Reset Filter and Search</button>
                    </div>
                </div>
            </div>

            {{-- Table --}}
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header text-white bg-success">
                            <h3 class="card-title">Users Table</h3>
                        </div>
                        <div class="card-body">
                            <input type="hidden" id="users_datatable_api_route" value="{{ route('covid19.api.users.datatable.list') }}">
                            <table id="users_table" class="table table-bordered table-striped table-hover w-100">
                                <thead>
                                    <tr>
                                        <th>Mobile Number</th>
                                        <th>Designation</th>
                                        <th>Status</th>
                                        <th>Declared Covid Positive At</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form method="post" id="update_user_form">
    <input type="hidden" name="row_id" id="row_id">
    <input type="hidden" name="user_id" id="user_id">
    <input type="hidden" name="designation" id="designation">
    <input type="hidden" id="update_user_url" value ="{{ route('covid19.api.users.update', '') }}">
</form>

<script>
var users_datatable;

$(function() {
    const options = { year: 'numeric', month: 'long', day: '2-digit' };
    const default_start_date = 'January 01, 2020';
    const default_end_date = new Date().toLocaleDateString("en-US", options);

    $('.select2bs4').select2();
    
    $('#start_date_range').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mmmm dd, yyyy',
        value: default_start_date
    });

    $('#end_date_range').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mmmm dd, yyyy',
        value: default_end_date
    });

    var users_datatable = $('#users_table').DataTable({
        "lengthChange": false,
        "searching": false,
        "pageLength": 10,
        "processing": true,
        "serverSide": true,
        "autoWidth": true,
        "order": [[ 3, "desc" ]],
        "ajax": {
            "url": $("#users_datatable_api_route").val(),
            "data": function ( data ) {
                data.start_date = $("#start_date_range").val();
                data.end_date = $("#end_date_range").val();
                data.status = $("#status_dropdown").val();
                data.keyword = $("#keyword").val();
            }
        },
        "columns": [
            { data: 'mobile_no', orderable: false, width: "22.5%"  },
            { data: 'designation', class: "designation", width: "22.5%" },
            { data: 'status', width: "22.5%" },
            { data: 'declared_covid_positive_at', width: "22.5%" },
            { data: 'update_btn', orderable: false, width: "10%" },
        ],
        "rowId": function(a) {
            return 'id_' + a.id;
        },
    });

    $("#users_table").on('click', 'tr', function () {
        var row_id = users_datatable.row(this).id();
        $("#row_id").val(row_id);
    });

    $(document).on('click', '.btn-update', function () {
        var row_id = $("#row_id").val();
        var designation = $(this).attr('data-designation');
        var user_id = $(this).attr('data-user-id');
        $("#user_id").val(user_id);
        var html = 
        '<select class="form-control" id="designation_dropdown">';

        if (designation == 1) {
            html +=
            '<option value="1" selected>Doctor</option>';
        } else {
            html +=
            '<option value="1">Doctor</option>';
        }

        if (designation == 2) {
            html +=
            '<option value="2" selected>Government Agent</option>';
        } else {
            html +=
            '<option value="2">Government Agent</option>';
        }
        
        if (designation == 3) {
            html +=
            '<option value="3" selected>User</option>';
        } else {
            html +=
            '<option value="3">User</option>';
        }

        html += '</select>'
        $("tr#" + row_id + " > td.designation").html(html);
        $(this).addClass('btn-save');
        $(this).removeClass('btn-update');
        $(this).html('Save');
        $(".btn-update").prop('disabled', true);
    });

    $(document).on('click', '.btn-save', function () {
        $("#update_user_form").submit();
    });

    $(document).on('change', '#designation_dropdown', function () {
        $("#designation").val($(this).val());
    });

    $(document).on('submit', '#update_user_form', function(e) {
        e.preventDefault();
        var formData = new FormData(this);

        $.ajax(
        {
            url: $("#update_user_url").val() + '/' + formData.get('user_id'),
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(output){
                var data = output.attributes;
                var designation = data.designation_name.replace(new RegExp('"', 'g'),'');
                var row_id = $("#row_id").val();
                $("tr#" + row_id + " > td.designation").html(designation);
                $(".btn-save").attr('data-designation', data.designation_id);
                $(".btn-save").addClass('btn-update');
                $(".btn-save").html('Update');
                $(".btn-save").removeClass('btn-save');
                $(".btn-update").prop('disabled', false);
            }						
        });
    });

    $('a[data-widget="pushmenu"]').on( 'click', function (e) {
        setTimeout(function(){
            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
        }, 300);
    });

    $("#start_date_range").on('change', function () {
        $("#keyword").val('');
        users_datatable.draw();
    })

    $("#end_date_range").on('change', function () {
        $("#keyword").val('');
        users_datatable.draw();
    })

    $("#status_dropdown").on('change', function () {
        $("#keyword").val('');
        users_datatable.draw();
    })

    $("#search_btn").on('click', function () {
        users_datatable.draw();
    })

    $("#reset_filter_btn").on('click', function () {
        $("#start_date_range").val(default_start_date);
        $("#end_date_range").val(default_end_date);
        $("#keyword").val('');
        $("#status_dropdown").val('all');
        $("#status_dropdown").change();
        users_datatable.draw();
    })
    
    $('#keyword').on('keyup', function(e) {
        if (e.which == 13) {
            $("#search_btn").click();
        }
    });
})

</script>
@endsection
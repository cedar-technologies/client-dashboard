@extends('covid19.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Hotspot Map</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('covid19.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Hotspot Map</li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div id="hotspot_map_section" class="row">
                                <div class="w-100" id="hotspot_map" style="min-height: calc(73vh);">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/bn/bn-all.js"></script>

<script>
$(function() {
    var data = [
        ['bn-te', 100],
        ['bn-be', 59],
        ['bn-bm', 225],
        ['bn-tu', 390]
    ];

// Create the chart
    Highcharts.mapChart('hotspot_map', {
        chart: {
            map: 'countries/bn/bn-all'
        },

        title: {
            text: 'Brunei Hotspot Map'
        },

        subtitle: {
            text: 'Infected users in Brunei'
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        colorAxis: {
            min: 0
        },

        series: [{
            data: data,
            name: 'Infected Users',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }],

        exporting: {
            enabled: false
        }
    });
})
</script>
@endsection
@extends('covid19.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">User Interactions</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('covid19.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">User Interactions</li>
                    </ol>
                </div>
            </div>
                                
            {{-- Search and Filter --}}
            <div class="row">
                <div class="col-md-2 col-6">
                    <label for="start_date_range">Date From:</label>
                    <div class="form-group">
                        <input name="start_date_range" id="start_date_range">
                    </div>
                </div>
                <div class="col-md-2 col-6">
                    <label for="end_date_range">Date To:</label>
                    <div class="form-group">
                        <input name="end_date_range" id="end_date_range">
                    </div>
                </div>
                <div class="col-md-2 col-12">
                    <div class="col-12">
                    <label for="distance_range">Min Distance (m):</label>
                    <div class="form-group mt-1">
                        <div class="slider-green">
                            <input type="text" value="" class="slider form-control" id="distance_range" name="distance_range"
                                data-slider-min="0" data-slider-max="{{ config('staticdata.covid19.distance_limit') }}" data-slider-step="0.1" 
                                data-slider-value="{{ config('staticdata.covid19.distance_limit') }}" data-slider-orientation="horizontal"
                                data-slider-selection="before" data-slider-tooltip="show">
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-2 col-12">
                    <label for="keyword">Search:</label>
                    <div class="input-group">
                        <input type="text" id="keyword" class="form-control" placeholder="Search mobile number.."
                            aria-label="Search mobile number.." aria-describedby="basic-addon2" value="{{ $notification_mobile_no }}">

                        <div class="input-group-append">
                            <button class="btn btn-success" id="search_btn" type="button" disabled><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-4 col-md-4 col-12">
                    <div class="web-view mt-md-4 pt-md-2 mt-3 pt-2"></div>
                    <div class="form-group">
                        <button class="w-100 btn btn-success" id="reset_filter_btn" type="button"><i class="fas fa-undo pr-2"></i>Reset Filter and Search</button>
                    </div>
                </div>
            </div>

            {{-- Table --}}
            <div class="row table-section" style="display: none">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header text-white bg-success">
                            <h3 class="card-title">User Interactions Table</h3>
                        </div>
                        <div class="card-body">
                            <input type="hidden" id="user_interactions_datatable_api_route" value="{{ route('covid19.api.user-interactions.datatable.list') }}">
                            <table id="user_interactions_table" class="table table-bordered table-striped table-hover w-100-responsive">
                                <thead>
                                    <tr>
                                        <th>Interaction</th>
                                        <th>Average Distance (m)</th>
                                        <th>Min Distance (m)</th>
                                        <th>Max Distance (m)</th>
                                        <th>Interaction Started At</th>
                                        <th>Interaction Duration (s)</th>
                                        <th>Location</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var user_interactions_datatable;
const distance_slider_max = $("#distance_range").attr('data-slider-max');

$(function() {
    const options = { year: 'numeric', month: 'long', day: '2-digit' };
    const default_start_date = 'January 01, 2020';
    const default_end_date = new Date().toLocaleDateString("en-US", options);

    $('.select2bs4').select2();
    var distance_slider = $('.slider').bootstrapSlider();
    
    $('#start_date_range').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mmmm dd, yyyy',
        value: default_start_date
    });

    $('#end_date_range').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mmmm dd, yyyy',
        value: default_end_date
    });

    user_interactions_datatable = $('#user_interactions_table').DataTable({
        "lengthChange": false,
        "searching": false,
        "pageLength": 10,
        "processing": true,
        "serverSide": true,
        "autoWidth": true,
        "scrollX": true,
        "order": [[ 5, "desc" ]],
        "ajax": {
            "url": $("#user_interactions_datatable_api_route").val(),
            "data": function ( data ) {
                data.start_date = $("#start_date_range").val();
                data.end_date = $("#end_date_range").val();
                data.keyword = $("#keyword").val();
                data.distance = $("#distance_range").val();
            }
        },
        "columns": [
            { data: 'mobile_no', orderable: false, width: "8%"  },
            { data: 'distance', width: "12%" },
            { data: 'min_distance', width: "11%" },
            { data: 'max_distance', width: "11%" },
            { data: 'interaction_started_at', width: "20%" },
            { data: 'duration', width: "14%" },
            { data: 'location', orderable: false, width: "12%" },
        ]
    });

    $('a[data-widget="pushmenu"]').on( 'click', function (e) {
        setTimeout(function(){
            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
        }, 300);
    });

    $("#start_date_range").on('change', function () {
        $("#keyword").val('');
        user_interactions_datatable.draw();
    })

    $("#end_date_range").on('change', function () {
        $("#keyword").val('');
        user_interactions_datatable.draw();
    })

    $("#distance_range").on('slideStop', function () {
        user_interactions_datatable.draw();
    })

    $("#search_btn").on('click', function () {
        user_interactions_datatable.draw();
        $(".table-section").animate({
            height: 'show'
        });
    })

    if ($("#keyword").val().length > 0) {
        $("#search_btn").prop('disabled', false);
        $("#search_btn").click();
    }

    $('#keyword').on('keyup', function(e) {
        if (e.which == 13) {
            $("#search_btn").click();
        }
        
        if ($(this).val().length == 0) {
            $("#search_btn").prop('disabled', true);
        } else {
            $("#search_btn").prop('disabled', false);
        }
    });

    $("#reset_filter_btn").on('click', function () {
        $("#start_date_range").val(default_start_date);
        $("#end_date_range").val(default_end_date);
        $("#keyword").val('');
        $("#search_btn").prop('disabled', true);
        distance_slider.bootstrapSlider('setValue', distance_slider_max);
        $(".table-section").animate({
            height: 'hide'
        });
        user_interactions_datatable.draw();
    })
})

</script>
@endsection
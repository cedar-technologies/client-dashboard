@extends('covid19.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('covid19.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div>
    </div>
    <!-- /.content-header -->
    <div class="content">
        <div class="container-fluid">
            <section class="col-lg-12">
                <!-- row -->
                <div class="row pt-1 pb-1">
                    <div class="col-12">
                        <div id="statistic" class="row">
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Infected users</span>
                                        <span class="info-box-number total-infected-users"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-people-arrows"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">User interactions</span>
                                        <span class="info-box-number total-user-interactions"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-percent"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Interaction-induced infections rate</span>
                                        <span class="info-box-number percentage-interaction-induced-infections"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-white bg-success">
                                <h3 class="card-title">
                                    <i class="fas fa-chart-line pr-2"></i>
                                    Infected Users Report
                                </h3>
                            </div>
                            <div class="card-body">
                                <div class="col-lg-2 col-md-3 col-12">
                                    <label for="infected_users_dropdown">Filter By:</label>
                                    <div class="form-group">
                                        <select class="form-control select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                                            name="infected_users_dropdown" id="infected_users_dropdown" style="width: 100% !important;">
                                            <option value="yearly" selected>Whole Year</option>
                                            <option value="weekly">Past 1 week</option>
                                            <option value="hourly">Past 24 hours</option>
                                        </select>
                                    </div>
                                </div>
                                 <input type="hidden" id="infected_users_chart_url" value="{{ route('covid19.api.charts.infected-users') }}"> 
                                <div id="infected_users_chart_section" class="row">
                                    <div class="w-100" id="infected_users_chart">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content-wrapper -->

<form id="header_form" method="get">
    <input type="hidden" id="header_data_url" value="{{ route('covid19.api.statistics.header-data') }}">
</form>
<form id="infected_users_chart_form" method="get"></form>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script>
var timer;
$(function () {
    $.widget.bridge('uibutton', $.ui.button)
    $('.select2bs4').select2();

    $("#header_form").submit();
    $("#infected_users_chart_form").submit();
})

$(document).on('submit', '#header_form', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    
    $.ajax(
    {
        url: $("#header_data_url").val(),
        type: "GET",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success:function(output){
            var data = output.attributes;
            $(".total-infected-users").html(data['total_infected_users']);
            $(".total-user-interactions").html(data['total_user_interactions']);
            $(".percentage-interaction-induced-infections").html(data['percentage_interaction_induced_infections'] + '%');
        }						
    });
});

$(document).on('submit', '#infected_users_chart_form', function(e) {
    e.preventDefault();
    clearInterval(timer);
    var formData = new FormData(this);
    
    $.ajax(
    {
        url: $("#infected_users_chart_url").val() + '?filter=' + $("#infected_users_dropdown").val(),
        type: "GET",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function(output){
            //$("#spinner2").hide();
            //$("#unit_completed_chart_section").show();
            var chart = initInfectedUsersChart(output.attributes);
            timer = setInterval(function () { $('#infected_users_chart_form').submit(); }, 60000);
        }						
    });
});

$(document).on('change', '#infected_users_dropdown', function() {
    //$("#unit_completed_chart_section").hide();
    //$("#spinner2").show();
    $("#infected_users_chart_form").submit();
})


function initInfectedUsersChart(response)
{
    var infected_users_chart = Highcharts.chart('infected_users_chart', {
        chart: {
            //type: 'area'
        },
        title: {
            text: response['title']
        },
        subtitle: {
            text: response['subtitle']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Infected Users'
            },
            align: 'middle'
        },
        xAxis: {
            type: 'datetime',
            tickInterval: response['x_axis'],
            text: 'days'
        },
        tooltip: {
            headerFormat: (function () {
                if (response['is_1_month']) {
                    return '<span style="font-size:10px">' + response['month_year'] + '</span><table>'
                }

                return '<span style="font-size:10px">{point.key}</span><table>';
            }()),
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            useHTML: true
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
        },
        series: [{
            name: 'Infected users',
            data: (function () {
                return populateInfectedUsersChartSeriesData(response['infected_users'], response['year'], $("#infected_users_dropdown").val());
            }()),
            color: '#dc3545',
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    }, function(chart) {
        var arr = chart.options.exporting.buttons.contextButton.menuItems;
        var index = arr.indexOf("viewData");
        if (index !== -1) arr.splice(index, 1);
        var index = arr.indexOf("openInCloud");
        if (index !== -1) arr.splice(index, 1);
        var index = arr.indexOf("separator");
        if (index !== -1) arr.splice(index, 1);
        var index = arr.indexOf("separator");
        if (index !== -1) arr.splice(index, 1);
    });
}

function populateInfectedUsersChartSeriesData(value, year, filter_type)
{
    var data = [];

    switch(filter_type) {
        case 'yearly':
            $.each(value, function (i, item) {
                data.push([
                    Date.UTC(parseInt(year), parseInt(item['month'])),
                    item['total']
                ]);
            });
            break;

        case 'weekly':
            $.each(value, function (i, item) {
                data.push([
                    Date.UTC(parseInt(year), parseInt(item['month']), parseInt(item['date'])),
                    item['total']
                ]);
            });
            break;

        case 'hourly':
            $.each(value, function (i, item) {
                data.push([
                    Date.UTC(parseInt(year), parseInt(item['month']), parseInt(item['date']), parseInt(item['hour'])),
                    item['total']
                ]);
            });
            break;

    }

    return data;
}

</script> 
@endsection
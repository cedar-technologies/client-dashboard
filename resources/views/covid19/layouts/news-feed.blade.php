@extends('covid19.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">News Feed</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('covid19.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">News Feed</li>
                    </ol>
                </div>
            </div>

            <div class="row">  
                <div class="col-8">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="col-12">
                                        <h2>What is happening?</h2>
                                        <textarea class="form-control" name="post" id="post" rows="5"></textarea>
                                        <button class="btn btn-lg btn-success float-right mt-2"><i class="fas fa-paper-plane pr-1"></i> Post</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card" style="min-height: 47vh">
                                <div class="card-body">
                                    <div class="col-12">
                                        <h2>News</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="row">
                        <div class="col-12">
                            <div class="card" style="min-height: 77.4vh">
                                <div class="card-body">
                                    <div class="col-12">
                                        <h2>News</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<input type="hidden" id="infection_tree_data_url" value="{{ route('covid19.api.infection-tree.data') }}">
<form id="infection_tree_form" method="get"></form>

<script>
var count = 0;
var oc;
$(function() {
    $.ajax(
    {
        url: $("#infection_tree_data_url").val() + '?mobile_no=' + $("#mobile_no").val(),
        type: "GET",
        processData: false,
        contentType: false,
        cache: false			
    });
})

$(document).on('click', "#search_btn", function () {
    if (count == 0) {
        initInfectionTree();
    } else {
        reinitInfectionTree();
    }
});

$(document).on('keyup', '#mobile_no', function(e) {
    if (e.which == 13) {
        $("#search_btn").click();
    }
});

function initInfectionTree()
{
    oc = $('#infection_tree').orgchart({
        'data' : $("#infection_tree_data_url").val() + '?mobile_no=' + $("#mobile_no").val(),
        'nodeContent': 'title',
        'pan': true,
        'zoom': true,
        'direction': 'l2r'
    });

    oc.$chartContainer.on('touchmove', function(event) {
        event.preventDefault();
      });
    count++;
}

function reinitInfectionTree()
{
    oc.init({ 'data': $("#infection_tree_data_url").val() + '?mobile_no=' + $("#mobile_no").val() });
}
</script>
@endsection
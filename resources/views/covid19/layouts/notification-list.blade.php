@extends('covid19.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Contact Tracing</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('covid19.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Contact Tracing</li>
                    </ol>
                </div>
            </div>

            <h4 class="text-success">Search Filter</h4>
            <hr>
            {{-- Search and Filter --}}
            <div class="row">
                <div class="col-md-2 col-6">
                    <label for="start_date_range">Date From:</label>
                    <div class="form-group">
                        <input name="start_date_range" id="start_date_range">
                    </div>
                </div>
                <div class="col-md-2 col-6">
                    <label for="end_date_range">Date To:</label>
                    <div class="form-group">
                        <input name="end_date_range" id="end_date_range">
                    </div>
                </div>
                <div class="col-md-2 col-12">
                    <label for="keyword">Search:</label>
                    <div class="input-group">
                        <input type="text" id="keyword" class="form-control" placeholder="Search mobile number.."
                            aria-label="Search mobile number.." aria-describedby="basic-addon2">

                        <div class="input-group-append">
                            <button class="btn btn-success" id="search_btn" type="button"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                    <div class="web-view mt-md-4 pt-md-2 mt-3 pt-2"></div>
                    <div class="form-group">
                        <button class="w-100 btn btn-success" id="reset_filter_btn" type="button"><i class="fas fa-undo pr-2"></i>Reset Filter and Search</button>
                    </div>
                </div>
            </div>

            <h4 class="text-success">Notification Filter</h4>
            <hr>

            <div class="row">
                <div class="col-md-2 col-6">
                    <label for="notification_duration_dropdown">Duration since positive:</label>
                    <div class="form-group">
                        <select class="custom-select select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                            name="notification_duration_dropdown" id="notification_duration_dropdown" style="width: 100% !important;">
                            @foreach (config('staticdata.covid19.notifications.filter.duration') as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-6">
                    <label for="notification_interaction_duration_dropdown">Interaction Duration:</label>
                    <div class="form-group">
                        <select class="custom-select select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                            name="notification_interaction_duration_dropdown" id="notification_interaction_duration_dropdown" style="width: 100% !important;">
                            @foreach (config('staticdata.covid19.notifications.filter.interaction_duration') as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-12">
                    <div class="col-12">
                    <label for="notification_distance_range">Min Distance (m):</label>
                    <div class="form-group mt-1">
                        <div class="slider-green">
                            <input type="text" value="" class="slider form-control" id="notification_distance_range" name="notification_distance_range"
                                data-slider-min="0" data-slider-max="{{ config('staticdata.covid19.distance_limit') }}" data-slider-step="0.1" 
                                data-slider-value="{{ config('staticdata.covid19.distance_limit') }}" data-slider-orientation="horizontal"
                                data-slider-selection="before" data-slider-tooltip="show">
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-12">
                    <div class="web-view mt-md-4 pt-md-2 mt-3 pt-2"></div>
                    <button class="btn btn-success btn-notify w-100" disabled>
                        <div id="notify_loading" style="display: none;">
                            <span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Notifying...
                        </div>
                        <div id="notify_default">
                            <i class="fas fa-bell pr-2"></i>Notify Selected Users Interactions
                        </div>
                    </button>
                </div>
            </div>

            {{-- Table --}}
            <div class="row mt-2">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header text-white bg-success">
                            <h3 class="card-title">Covid Positive Users Table</h3>
                        </div>
                        <div class="card-body">
                            <input type="hidden" id="users_datatable_api_route" value="{{ route('covid19.api.notifications.datatable.list') }}">
                            <table id="users_table" class="table table-bordered table-striped table-hover w-100">
                                <thead>
                                    <tr>
                                        <th>Mobile Number</th>
                                        <th>Designation</th>
                                        <th>Declared Covid Positive At</th>
                                        <th>Notify</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="notify_success_modal" tabindex="-1" role="dialog" aria-labelledby="notify_success_modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header text-white">
            <h5 class="modal-title text-center">Push Notification Sent</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p class="text-center">
                Users who have been interacted with selected users have been notified
            </p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fas fa-times pr-2"></i> Close</button>
        </div>
    </div>
    </div>
</div>

<form id="notify_form">
    <input type="hidden" id="notify_url" value ="{{ route('covid19.api.notifications.notify') }}">
</form>

<script>
var users_datatable;
var notify_mobile_no_list = [];

$(function() {
    const options = { year: 'numeric', month: 'long', day: '2-digit' };
    const default_start_date = 'January 01, 2020';
    const default_end_date = new Date().toLocaleDateString("en-US", options);

    $('.select2bs4').select2();
    $('.slider').bootstrapSlider();
    
    $('#start_date_range').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mmmm dd, yyyy',
        value: default_start_date
    });

    $('#end_date_range').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mmmm dd, yyyy',
        value: default_end_date
    });

    var users_datatable = $('#users_table').DataTable({
        "lengthChange": false,
        "searching": false,
        "pageLength": 10,
        "processing": true,
        "serverSide": true,
        "autoWidth": true,
        "scrollX": true,
        "order": [[ 2, "desc" ]],
        "ajax": {
            "url": $("#users_datatable_api_route").val(),
            "data": function ( data ) {
                data.start_date = $("#start_date_range").val();
                data.end_date = $("#end_date_range").val();
                data.keyword = $("#keyword").val();
            }
        },
        "columns": [
            { data: 'mobile_no', orderable: false, width: "30%"  },
            { data: 'designation', width: "30%" },
            { data: 'declared_covid_positive_at', width: "30%" },
            { data: 'notify_checkbox', class: "icheck-success text-center", orderable: false, width: "10%" },
        ]
    });

    $('a[data-widget="pushmenu"]').on( 'click', function (e) {
        setTimeout(function(){
            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
        }, 300);
    });

    $("#start_date_range").on('change', function () {
        $("#keyword").val('');
        users_datatable.draw();
    })

    $("#end_date_range").on('change', function () {
        $("#keyword").val('');
        users_datatable.draw();
    })

    $("#search_btn").on('click', function () {
        users_datatable.draw();
    })

    $("#reset_filter_btn").on('click', function () {
        $("#start_date_range").val(default_start_date);
        $("#end_date_range").val(default_end_date);
        $("#keyword").val('');
        users_datatable.draw();
    })
    
    $('#keyword').on('keyup', function(e) {
        if (e.which == 13) {
            $("#search_btn").click();
        }
    });

    $(document).on('change', '.notify-checkbox', function () {
        var is_checked = $(this).is(':checked');
        var mobile_no = $(this).attr('data-mobile_no');

        if (is_checked) {
            notify_mobile_no_list.push(mobile_no);
            $(".btn-notify").prop('disabled', false);
        } else {
            notify_mobile_no_list.splice($.inArray(mobile_no, notify_mobile_no_list), 1);
            if (!notify_mobile_no_list.length) {
                $(".btn-notify").prop('disabled', true);
            }
        }
    });

    $(document).on('click', '.btn-notify', function () {
        $("#notify_loading").show();
        $("#notify_default").hide();
        $("#notify_form").submit();
    });

    $(document).on('submit', '#notify_form', function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        formData.append('mobile_no', JSON.stringify(notify_mobile_no_list));
        formData.append('duration', $("#notification_duration_dropdown").val());
        formData.append('interaction_duration', $("#notification_interaction_duration_dropdown").val());
        formData.append('min_distance', $("#notification_distance_range").val());
        
        $.ajax(
        {
            url: $("#notify_url").val(),
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(output){
                var message = output.message;
                
                setTimeout(
                    function() 
                    {
                        $("#notify_loading").hide();
                        $("#notify_default").show();
                        $('.notify-checkbox').prop('checked',false);
                        $(".btn-notify").prop('disabled', true);
                        $("#notify_success_modal").modal('toggle');
                    }, 2500
                );
            }
        });
    });

})

</script>
@endsection
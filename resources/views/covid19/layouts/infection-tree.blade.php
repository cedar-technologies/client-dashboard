@extends('covid19.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Infection Tree</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('covid19.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Infection Tree</li>
                    </ol>
                </div>
            </div>

            {{-- Search and Filter --}}
            <div class="row">
                <div class="col-12">
                    <div class="input-group">
                        <input type="text" id="mobile_no" class="form-control" placeholder="Search mobile number.."
                            aria-label="Search mobile number.." aria-describedby="basic-addon2" autofocus>

                        <div class="input-group-append">
                            <button class="btn btn-success" id="search_btn" type="button"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div id="infection_tree" class="text-left"></div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="infection_tree_data_url" value="{{ route('covid19.api.infection-tree.data') }}">
<form id="infection_tree_form" method="get"></form>

<script>
var count = 0;
var oc;
$(function() {
    $.ajax(
    {
        url: $("#infection_tree_data_url").val() + '?mobile_no=' + $("#mobile_no").val(),
        type: "GET",
        processData: false,
        contentType: false,
        cache: false			
    });
})

$(document).on('click', "#search_btn", function () {
    if (count == 0) {
        initInfectionTree();
    } else {
        reinitInfectionTree();
    }
});

$(document).on('keyup', '#mobile_no', function(e) {
    if (e.which == 13) {
        $("#search_btn").click();
    }
});

function initInfectionTree()
{
    oc = $('#infection_tree').orgchart({
        'data' : $("#infection_tree_data_url").val() + '?mobile_no=' + $("#mobile_no").val(),
        'nodeContent': 'title',
        'pan': true,
        'zoom': true,
        'direction': 'l2r'
    });

    oc.$chartContainer.on('touchmove', function(event) {
        event.preventDefault();
      });
    count++;
}

function reinitInfectionTree()
{
    oc.init({ 'data': $("#infection_tree_data_url").val() + '?mobile_no=' + $("#mobile_no").val() });
}
</script>
@endsection
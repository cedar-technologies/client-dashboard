<!DOCTYPE html>
<html lang="en" class="gr__adminlte_io">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>COVID-19 Tracking</title>
    <script src="https://kit.fontawesome.com/7f357bb1b5.js" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/app.js') }}"></script>
    
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <div class="wrapper">
        @include('covid19.header')

        @include('covid19.sidebar') 

        <section class="content">
            @yield('content')
        </section>

        @include('covid19.footer')
    </div>
</body>
</html>

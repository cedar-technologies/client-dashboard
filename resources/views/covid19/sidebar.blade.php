
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-success elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('covid19.home') }}" class="brand-link">
      <img src="/img/cedar-logo.png" alt="Cedar Technologies Logo" class="brand-image">
      <span class="brand-text font-weight-light">COVID-19 Tracking</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/img/avatar-blank.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('covid19.home') }}" class="nav-link {{ session('active_nav') == 'dashboard' ? 'active' : null }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('covid19.users.list') }}" class="nav-link {{ session('active_nav') == 'users' ? 'active' : null }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('covid19.user-interactions.list') }}" class="nav-link {{ session('active_nav') == 'user-interactions' ? 'active' : null }}">
              <i class="nav-icon fas fa-people-arrows"></i>
              <p>
                User Interactions
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('covid19.infection-tree') }}" class="nav-link {{ session('active_nav') == 'infection-tree' ? 'active' : null }}">
              <i class="nav-icon fas fa-tree"></i>
              <p>
                Infection Tree
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('covid19.news-feed.home') }}" class="nav-link {{ session('active_nav') == 'news-feed' ? 'active' : null }}">
              <i class="nav-icon fas fa-newspaper"></i>
              <p>
                News Feed
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('covid19.hotspot-map.home') }}" class="nav-link {{ session('active_nav') == 'hotspot-map' ? 'active' : null }}">
              <i class="nav-icon fas fa-globe-asia"></i>
              <p>
                Hotspot Map
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('covid19.notification.list') }}" class="nav-link {{ session('active_nav') == 'notification-list' ? 'active' : null }}">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                Contact Tracing
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
<footer class="main-footer">
    <strong>Copyright &copy; {{ \Carbon\Carbon::now()->year }} <a target="_blank" href="https://cedartech.com.my">Cedar Technologies</a>.</strong> All rights reserved.
</footer>
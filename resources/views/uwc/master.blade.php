<!DOCTYPE html>
<html lang="en" class="gr__adminlte_io">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>UWC</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/app.js') }}"></script>
    
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('uwc.header')

        @include('uwc.sidebar')

        <section class="content">
            @yield('content')
        </section>

        @include('uwc.footer')
    </div>
</body>
</html>

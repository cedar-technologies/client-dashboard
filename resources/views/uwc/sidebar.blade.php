
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-success elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('uwc.home') }}" class="brand-link">
      <img src="/img/cedar-logo.png" alt="Cedar Technologies Logo" class="brand-image">
      <span class="brand-text font-weight-light">UWC</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/img/avatar-blank.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('uwc.home') }}" class="nav-link {{ session('active_nav') == 'dashboard' ? 'active' : null }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('uwc.employees.view.list') }}" class="nav-link {{ session('active_nav') == 'employees' ? 'active' : null }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Employee Management
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('uwc.employee-shifts.view.list') }}" class="nav-link {{ session('active_nav') == 'employee-shifts' ? 'active' : null }}">
              <i class="nav-icon fas fa-user-check"></i>
              <p>
                Employee Shifts
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('uwc.production-line-analytics.view.index') }}" class="nav-link {{ session('active_nav') == 'production-line-analytics' ? 'active' : null }}">
              <i class="nav-icon fas fa-chart-line"></i>
              <p>
                Production Line Analytics
                <!-- <span class="right badge badge-danger">New</span> -->
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('uwc.defects.view.list') }}" class="nav-link {{ session('active_nav') == 'defects' ? 'active' : null }}">
              <i class="nav-icon fas fa-trash"></i>
              <p>
                Defects
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('uwc.stations.view.list') }}" class="nav-link {{ session('active_nav') == 'traceabilities' ? 'active' : null }}">
              <i class="nav-icon fas fa-briefcase"></i>
              <p>
                Traceabilities
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('uwc.reports.view.index') }}" class="nav-link {{ session('active_nav') == 'reports' ? 'active' : null }}">
              <i class="nav-icon fas fa-file-alt"></i>
              <p>
                Generate Report
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
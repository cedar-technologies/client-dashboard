@extends('uwc.master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('uwc.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="content">
        <div class="container-fluid">
            <section class="col-lg-12">
                <!-- row -->
                <div class="row pt-1 pb-1">
                    <div class="col-12">
                        <!-- jQuery Knob -->
                        <div class="card">
                            <div class="card-header text-white bg-success">
                                <h3 class="card-title">
                                    <i class="fas fa-tachometer-alt pr-2"></i> 
                                    Daily Report
                                </h3>
                            </div>
                            <div class="card-body">
                                <div id="spinner1" class="text-center">
                                    <img src="/img/loading-spinner.gif" alt="loading.." style="height: 100%">
                                </div>
                                <div id="daily_percentage" class="row" style="display: none;">
                                    <input type="hidden" id="daily_percentage_chart_url" value="{{ route('uwc.api.charts.daily-percentage') }}">
                                    <div class="col-6 col-md-3 text-center">
                                        <i class="fas fa-clock fa-2x pb-3 daily-average-icon"></i><br>
                                        <input id="working_hours" type="text" class="knob" value="" 
                                            data-width="120" data-height="120" data-fgColor="#28a745"
                                            data-min="0" data-max="12" data-readOnly=true>

                                        <div class="knob-label">Daily average working hours</div>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <i class="fas fa-business-time fa-2x pb-3 daily-average-icon"></i><br>
                                        <input id="total_units" type="text" class="knob-total-units" value="" 
                                            data-width="120" data-height="120" data-fgColor="orange"
                                            data-min="0" data-max="100000" data-readOnly=true>

                                        <div class="knob-label">Daily total units completed</div>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <i class="fas fa-tasks fa-2x pb-3 daily-average-icon"></i><br>
                                        <input id="average_units" type="text" class="knob-avg-units-per-hour" value="" data-width="120"
                                            data-height="120" data-fgColor="#11B5E6"
                                            data-min="0" data-readOnly=true>

                                        <div class="knob-label">Daily average units completed per hour</div>
                                    </div>
                                    <div class="col-6 col-md-3 text-center">
                                        <i class="fas fa-dumpster fa-2x pb-3 daily-average-icon"></i><br>
                                        <input id="defects_percentage" type="text" class="knob-defects-percentage" value=""
                                            data-width="120" data-height="120" data-fgColor="red"
                                            data-min="0" data-max="100" >

                                        <div class="knob-label">Daily defects percentage</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-white bg-success">
                                <h3 class="card-title">
                                    <i class="far fa-chart-bar pr-2"></i>
                                    Unit Completion Report
                                </h3>
                            </div>
                            <div class="card-body">
                                <div class="col-lg-2 col-md-3 col-12">
                                    <label for="chart1_dropdown">Filter By:</label>
                                    <div class="form-group">
                                        <select class="form-control select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                                            name="chart1_dropdown" id="chart1_dropdown" style="width: 100% !important;">
                                            <option value="yearly" selected>Whole Year</option>
                                            <option value="weekly">Past 1 week</option>
                                            <option value="hourly">Past 24 hours</option>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" id="unit_completed_chart_url" value="{{ route('uwc.api.charts.unit-completed') }}">
                                <div id="spinner2" class="text-center">
                                    <img src="/img/loading-spinner.gif" alt="loading..">
                                </div>
                                <div id="unit_completed_chart_section" class="row" style="display: none;">
                                    <div class="w-100" id="unit_completed_chart">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header text-white bg-success">
                                <h3 class="card-title">
                                    <i class="fas fa-chart-line pr-2"></i>
                                    Defect Report
                                </h3>
                            </div>
                            <div class="card-body">
                                <div class="col-lg-2 col-md-3 col-12">
                                    <label for="chart2_dropdown">Filter By:</label>
                                    <div class="form-group">
                                        <select class="form-control select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                                            name="chart1_dropdown" id="chart2_dropdown" style="width: 100% !important;">
                                            <option value="yearly" selected>Whole Year</option>
                                            <option value="weekly">Past 1 week</option>
                                            <option value="hourly">Past 24 hours</option>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" id="defects_chart_url" value="{{ route('uwc.api.charts.defects') }}">
                                <div id="spinner3" class="text-center">
                                    <img src="/img/loading-spinner.gif" alt="loading..">
                                </div>
                                <div id="defects_chart_section" class="row" style="display: none;">
                                    <div class="w-100" id="defects_chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content-wrapper -->

<form id="daily_percentage_chart_form" method="get"></form>
<form id="unit_completed_chart_form" method="get"></form>
<form id="defects_chart_form" method="get"></form>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script>
var timer;
$(function () {
    $.widget.bridge('uibutton', $.ui.button)
    $('.select2bs4').select2();

    $("#daily_percentage_chart_form").submit();
    $("#unit_completed_chart_form").submit();
    $("#defects_chart_form").submit();
})

$(document).on('submit', '#daily_percentage_chart_form', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    
    $.ajax(
    {
        url: $("#daily_percentage_chart_url").val(),
        type: "GET",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success:function(output){
            $("#spinner1").hide();
            $("#daily_percentage").show();
            initDailyPercentageChart(output.attributes);
        }						
    });
});

$(document).on('submit', '#unit_completed_chart_form', function(e) {
    e.preventDefault();
    clearInterval(timer);
    var formData = new FormData(this);
    
    $.ajax(
    {
        url: $("#unit_completed_chart_url").val() + '?filter=' + $("#chart1_dropdown").val(),
        type: "GET",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function(output){
            $("#spinner2").hide();
            $("#unit_completed_chart_section").show();
            var chart = initUnitsCompletedChart(output.attributes);
            timer = setInterval(function () { $('#unit_completed_chart_form').submit(); }, 60000);
        }						
    });
});

$(document).on('submit', '#defects_chart_form', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    
    $.ajax(
    {
        url: $("#defects_chart_url").val() + '?filter=' + $("#chart2_dropdown").val(),
        type: "GET",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success:function(output){
            $("#spinner3").hide();
            $("#defects_chart_section").show();
            var chart = initDefectsChart(output.attributes);
        }						
    });
});

$(document).on('change', '#chart1_dropdown', function() {
    $("#unit_completed_chart_section").hide();
    $("#spinner2").show();
    $("#unit_completed_chart_form").submit();
})

$(document).on('change', '#chart2_dropdown', function() {
    $("#defects_chart_section").hide();
    $("#spinner3").show();
    $("#defects_chart_form").submit();
})

function initDailyPercentageChart(response)
{
    $("#working_hours").val(response['working_hours']);
    $("#total_units").val(response['total_units']);
    $("#average_units").val(response['total_avg_units_per_hour']);
    $("#defects_percentage").val(response['defects_percentage']);
    var max_avg_units_per_hour = response['avg_units_per_hour'] * response['total_station'] * response['total_production_line'];
    var max_total_units = max_avg_units_per_hour * 24;

    $(".knob").knob({
        'format' : function (value) {
            return formatKnobWorkingHours($("#working_hours").val());
        },
        'draw': function () {
            $(this.i).css('font-size', '15pt');
        }
    });
    
    $(".knob-total-units").knob({
        'format': function (value) {
            return formatKnobTotalUnits($("#total_units").val());
        },
        'max': max_total_units,
        'draw': function () {
            $(this.i).css('font-size', '15pt');
        }
    });
    
    $(".knob-avg-units-per-hour").knob({
        'format' : function (value) {
            return formatKnobAvgUnitsPerHour($("#average_units").val());
        },
        'max' : max_avg_units_per_hour,
        'draw': function () {
            $(this.i).css('font-size', '15pt');
        }
    });
    
    $(".knob-defects-percentage").knob({
        'format' : function (value) {
            return formatKnobDefectsPercentage($("#defects_percentage").val());
        },
        'draw': function () {
            $(this.i).css('font-size', '15pt');
        }
    });
}

function initUnitsCompletedChart(response)
{
    var unit_completed_chart = Highcharts.chart('unit_completed_chart', {
        chart: {
            // type: 'column'
        },
        title: {
            text: response['title']
        },
        subtitle: {
            text: response['subtitle']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Units Completed'
            },
            align: 'middle'
        },
        xAxis: {
            type: 'datetime',
            tickInterval: response['x_axis'],
            text: 'days'
        },
        tooltip: {
            headerFormat: (function () {
                if (response['is_1_month']) {
                    return '<span style="font-size:10px">' + response['month_year'] + '</span><table>'
                }

                return '<span style="font-size:10px">{point.key}</span><table>';
            }()),
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} units</b></td></tr>',
            footerFormat: '</table>',
            useHTML: true
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
        },
        series: [{
            name: 'Station 1',
            data: (function () {
                return populateUnitsCompletedChartSeriesData(response['station1'], response['year'], $("#chart1_dropdown").val());
            }()),
        }, {
            name: 'Station 2',
            data: (function () {
                return populateUnitsCompletedChartSeriesData(response['station2'], response['year'], $("#chart1_dropdown").val());
            }()),
        }, {
            name: 'Station 3',
            data: (function () {
                return populateUnitsCompletedChartSeriesData(response['station3'], response['year'], $("#chart1_dropdown").val());
            }()),
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    }, function(chart) {
        var arr = chart.options.exporting.buttons.contextButton.menuItems;
        var index = arr.indexOf("viewData");
        if (index !== -1) arr.splice(index, 1);
        var index = arr.indexOf("openInCloud");
        if (index !== -1) arr.splice(index, 1);
        var index = arr.indexOf("separator");
        if (index !== -1) arr.splice(index, 1);
        var index = arr.indexOf("separator");
        if (index !== -1) arr.splice(index, 1);
    });
}

function initDefectsChart(response)
{
    var defects_chart = Highcharts.chart('defects_chart', {
        chart: {
            // type: 'column'
        },
        title: {
            text: response['title']
        },
        subtitle: {
            text: response['subtitle']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of defective units'
            },
            align: 'middle'
        },
        xAxis: {
            type: 'datetime',
            tickInterval: response['x_axis'],
            text: 'days'
        },
        tooltip: {
            headerFormat: (function () {
                if (response['is_1_month']) {
                    return '<span style="font-size:10px">' + response['month_year'] + '</span><table>'
                }

                return '<span style="font-size:10px">{point.key}</span><table>';
            }()),
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} units</b></td></tr>',
            footerFormat: '</table>',
            useHTML: true
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
        },
        series: (function () {
            return populateDefectsChartSeriesData(response['defects'], response['year'], $("#chart2_dropdown").val());
        }()),
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    });
}

function countDecimals(value)
{
    if(Math.floor(value) == value) return 0;
    return value.toString().split(".")[1].length || 0; 
}

function formatKnobWorkingHours(value)
{
    decimal = countDecimals(value);
    
    if (decimal > 2) {
        value = parseFloat(value);
        value = value.toFixed(2);
    }

    return value;
}

function formatKnobTotalUnits(value)
{
    if (value < 1000000 && value > 0) {
        value = value / 1000;
        decimal = countDecimals(value);
        if (decimal > 2) {
            value = parseFloat(value);
            value = value.toFixed(2) + 'k';
        } else {
            value += 'k';
        }
    } else if (value >= 1000000) {
        value = value / 1000000;
        decimal = countDecimals(value);
        if (decimal > 2) {
            value = parseFloat(value);
            value = value.toFixed(2) + 'm';
        } else {
            value += 'm';
        }
    }

    return value;
}

function formatKnobAvgUnitsPerHour(value)
{
    decimal = countDecimals(value);
    if (value >= 1000) {
        value = value / 1000;
        if (decimal > 2) {
            value = parseFloat(value);
            value = value.toFixed(2) + 'k';
        } else {
            value += 'k';
        }
    } else if (value < 1000) {
        if (decimal > 2) {
            value = parseFloat(value);
            value = value.toFixed(2);
        }
    }

    return value;
}

function formatKnobDefectsPercentage(value)
{
    decimal = countDecimals(value);
    if (decimal > 2) {
        value = parseFloat(value);
        value = value.toFixed(2) + '%';
    } else {
        value += '%';
    }

    return value;
}

function populateUnitsCompletedChartSeriesData(value, year, filter_type)
{
    var data = [];

    switch(filter_type) {
        case 'yearly':
            $.each(value, function (i, item) {
                data.push([
                    Date.UTC(parseInt(year), parseInt(item['month'])),
                    item['units']
                ]);
            });
            break;

        case 'weekly':
            $.each(value, function (i, item) {
                data.push([
                    Date.UTC(parseInt(year), parseInt(item['month']), parseInt(item['date'])),
                    item['units']
                ]);
            });
            break;

        case 'hourly':
            $.each(value, function (i, item) {
                data.push([
                    Date.UTC(parseInt(year), parseInt(item['month']), parseInt(item['date']), parseInt(item['hour'])),
                    item['units']
                ]);
            });
            break;

    }

    return data;
}

function populateDefectsChartSeriesData(value, year, filter_type)
{
    var series_data = [];

    switch(filter_type) {
        case 'yearly':
            $.each(value, function (i, item1) {
                series_data.push({
                    name: i,
                    data: (function () {
                        var data = [];
                        
                        $.each(value[i], function (j, item2) {
                            data.push([
                                Date.UTC(parseInt(year), parseInt(item2['month'])),
                                item2['units']
                            ]);
                        });

                        return data;
                    }()),
                });
            });
            break;

        case 'weekly':
            $.each(value, function (i, item1) {
                series_data.push({
                    name: i,
                    data: (function () {
                        var data = [];
                        
                        $.each(value[i], function (j, item2) {
                            data.push([
                                Date.UTC(parseInt(year), parseInt(item2['month']), parseInt(item2['date'])),
                                item2['units']
                            ]);
                        });

                        return data;
                    }()),
                });
            });
            break;

        case 'hourly':
            $.each(value, function (i, item1) {
                series_data.push({
                    name: i,
                    data: (function () {
                        var data = [];
                        
                        $.each(value[i], function (j, item2) {
                            data.push([
                                Date.UTC(parseInt(year), parseInt(item2['month']), parseInt(item2['date']), parseInt(item2['hour'])),
                                item2['units']
                            ]);
                        });

                        return data;
                    }()),
                });
            });
            break;
        console.log(series_data);
        
    }

    return series_data;
}
// Test zoomable
function initTestZoomableChart() {
    Highcharts.getJSON('http://localhost:1000/json/test-data.json', function (data) {
        // create the chart
        Highcharts.stockChart('unit_completed_chart', {
            chart: {
                alignTicks: false
            },

            rangeSelector: {
                selected: 1
            },
            
            navigator: {
                adaptToUpdatedData: false,
                series: {
                    data: data
                }
            },

            scrollbar: {
                liveRedraw: false
            },

            // rangeSelector: {
            //     buttons: [{
            //         type: 'hour',
            //         count: 1,
            //         text: '1h'
            //     }, {
            //         type: 'day',
            //         count: 1,
            //         text: '1d'
            //     }, {
            //         type: 'month',
            //         count: 1,
            //         text: '1m'
            //     }, {
            //         type: 'year',
            //         count: 1,
            //         text: '1y'
            //     }, {
            //         type: 'all',
            //         text: 'All'
            //     }],
            //     // inputEnabled: false, // it supports only days
            //     selected: 4 // all
            // },

            title: {
                text: 'AAPL Stock Volume'
            },

            series: [{
                type: 'column',
                name: 'AAPL Stock Volume',
                data: data,
                dataGrouping: {
                    enabled: false
                    // units: [[
                    //     'week', // unit name
                    //     [1] // allowed multiples
                    // ], [
                    //     'month',
                    //     [1, 2, 3, 4, 6]
                    // ]]
                }
            }]
        });
    });
}

function initTestZoomableChart2()
{
    // See source code from the JSONP handler at https://github.com/highcharts/highcharts/blob/master/samples/data/from-sql.php
    $.getJSON('https://www.highcharts.com/samples/data/from-sql.php?callback=?', function (data) {

        // Add a null value for the end date
        data = [].concat(data, [[Date.UTC(2011, 9, 14, 19, 59), null, null, null, null]]);

        // create the chart
        Highcharts.stockChart('unit_completed_chart', {
            chart: {
                type: 'candlestick',
                zoomType: 'x'
            },

            navigator: {
                adaptToUpdatedData: false,
                series: {
                    data: data
                }
            },

            scrollbar: {
                liveRedraw: false
            },

            title: {
                text: 'AAPL history by the minute from 1998 to 2011'
            },

            subtitle: {
                text: 'Displaying 1.7 million data points in Highcharts Stock by async server loading'
            },

            rangeSelector: {
                buttons: [{
                    type: 'hour',
                    count: 1,
                    text: '1h'
                }, {
                    type: 'day',
                    count: 1,
                    text: '1d'
                }, {
                    type: 'month',
                    count: 1,
                    text: '1m'
                }, {
                    type: 'year',
                    count: 1,
                    text: '1y'
                }, {
                    type: 'all',
                    text: 'All'
                }],
                inputEnabled: false, // it supports only days
                selected: 4 // all
            },

            xAxis: {
                events: {
                    afterSetExtremes: afterSetExtremes
                },
                minRange: 3600 * 1000 // one hour
            },

            yAxis: {
                floor: 0
            },

            series: [{
                data: data,
                dataGrouping: {
                    enabled: false
                }
            }]
        });
    });
}

/**
 * Load new data depending on the selected min and max
 */
function afterSetExtremes(e) {

    var chart = Highcharts.charts[0];

    chart.showLoading('Loading data from server...');
    $.getJSON('https://www.highcharts.com/samples/data/from-sql.php?start=' + Math.round(e.min) +
            '&end=' + Math.round(e.max) + '&callback=?', function (data) {

        chart.series[0].setData(data);
        chart.hideLoading();
    });
}
</script> 
@endsection
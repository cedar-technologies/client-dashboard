@extends('uwc.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Employee Management</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('uwc.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Employee Management</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-12">
                    <label for="search_dropdown">Search By:</label>
                    <div class="input-group">
                        <select class="custom-select select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                            name="search_dropdown" id="search_dropdown" style="width: 50%">
                        <option value="" selected disabled>Select Category</option>
                        @foreach (config('staticdata.employees.search-dropdown') as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                        </select>

                        <input type="text" id="keyword" class="form-control" placeholder="Search keyword.." disabled
                            aria-label="Search keyword.." aria-describedby="basic-addon2" style="border-radius: 0%;">

                        <div class="input-group-append">
                            <button class="btn btn-success" id="search_btn" type="button" disabled><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1 col-md-2 col-12">
                    <div class="mt-md-4 pt-md-2 mt-3 pt-2"></div>
                    <div class="form-group">
                        <button class="btn btn-success w-100" id="reset_filter_btn" type="button"><i class="fas fa-undo pr-2"></i>Reset</button>
                    </div>
                </div>
                @if (auth()->user()->isAdmin())
                    <div class="col-xl-2 col-lg-4 col-md-4 col-12 mb-md-0 mb-3">
                        <div class="mt-md-4 pt-md-2"></div>
                        <a id="add_new_employee" href="{{ route('uwc.employees.view.create') }}" class="btn btn-success w-100">
                            <i class="fas fa-plus pr-2"></i>
                            Add New Employee
                        </a>
                    </div>
                @endif
            </div>
            
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header text-white bg-success">
                            <h3 class="card-title">Employee Table</h3>
                        </div>
                        <div class="card-body">
                            <input type="hidden" id="employees_datatable_api_route" value="{{ route('uwc.api.employees.datatable.list') }}">
                            <table id="employee_table" class="table table-bordered table-striped table-hover w-100">
                                <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>Full Name</th>
                                        <th>Card Number</th>
                                        <th>Total Working Hours</th>
                                        @if (auth()->user()->isAdmin())
                                            <th class="text-center"><i class="fas fa-trash-alt"></i></th>
                                        @endif
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="employee_count" value="{{ count($employee_list) }}">
<input type="hidden" id="employee_limit" value="{{ config('staticdata.employee_limit') }}">
<input type="hidden" id="is_admin" value="{{ $is_admin }}">

<!-- Modal -->
<div class="modal fade" id="add_limit_alert_modal" tabindex="-1" role="dialog" aria-labelledby="add_limit_alert_modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header text-white">
            <h5 class="modal-title text-center">Employee Limit Reached</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p class="text-center">
                You have reached maximum number of employees, please contact our company's representative to add more employee
            </p> 
            <p class="text-center">
                <b>Current employee limit: {{ config('staticdata.employee_limit') }}</b>
            </p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-dark" data-dismiss="modal"><i class="fas fa-times pr-2"></i> Close</button>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="delete_modal" aria-hidden="true">
    <div class="modal-sm modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header text-white">
            <h5 class="modal-title text-center">Confirm Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p class="text-center">Are you sure to delete this employee?</p> 
            
            <form method="POST" id="delete_employee_form">
                @csrf
                <input type="hidden" id="delete_employee_id" value="">
                <input type="hidden" id="delete_url" value="{{ route('uwc.employees.delete', ['id' => 1]) }}">
                <input type="hidden" name="_method" value="delete" />
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" id="confirm_delete_btn" class="btn btn-success"><i class="fas fa-check pr-2"></i>Confirm</button>
            <button type="button" class="btn btn-dark" data-dismiss="modal"><i class="fas fa-times pr-2"></i> Close</button>
        </div>
    </div>
    </div>
</div>

<script>
var employee_datatable = '';

$(function() {
    let is_admin = $("#is_admin").val();

    $('.select2bs4').select2();
    $(".select2-selection").css({"border-top-right-radius": "0%", "border-bottom-right-radius": "0%"});

    if (is_admin == "true") {
        employee_datatable = $('#employee_table').DataTable({
            "lengthChange": false,
            "searching": false,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "order": [[ 0, "desc" ]],
            "ajax": {
                "url": $("#employees_datatable_api_route").val(),
                "data": function ( data ) {
                    data.is_admin = true,
                    data.search_by = $("#search_dropdown").val();
                    data.keyword = $("#keyword").val();
                }
            },
            "columns": [
                { data: 'employee_id' },
                { data: 'name' },
                { data: 'card_no' },
                { data: 'total_working_hours', orderable: false },
                { data: 'delete', orderable: false, class: 'text-center' }
            ]
        });
    } else {
        employee_datatable = $('#employee_table').DataTable({
            "lengthChange": false,
            "searching": false,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "autoWidth": true,
            "order": [[ 0, "desc" ]],
            "ajax": {
                "url": $("#employees_datatable_api_route").val(),
                "data": function ( data ) {
                    data.is_admin = false,
                    data.search_by = $("#search_dropdown").val();
                    data.keyword = $("#keyword").val();
                }
            },
            "columns": [
                { data: 'employee_id' },
                { data: 'name' },
                { data: 'card_no' },
                { data: 'total_working_hours', orderable: false },
            ]
        });
    }
    
    $('a[data-widget="pushmenu"]').on( 'click', function (e) {
        setTimeout(function(){
            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
        }, 300);
    });

    $("#search_btn").on('click', function () {
        employee_datatable.draw();
    })

    $("#reset_filter_btn").on('click', function () {
        $("#search_dropdown").val('');
        $("#search_dropdown").change();
        $("#keyword").val('');
        employee_datatable.draw();
    })
    
    $('#keyword').on('keyup', function(e) {
        if (e.which == 13) {
            $("#search_btn").click();
        }
        
        if ($(this).val().length == 0) {
            $("#search_btn").prop('disabled', true);
        } else {
            $("#search_btn").prop('disabled', false);
        }
    });
    
    $('#search_dropdown').on('change', function(e) {
        if (!$(this).val()) {
            $("#keyword").prop('disabled', true);
        } else {
            $("#keyword").prop('disabled', false);
        }
    });
})

$(document).on('click', '#add_new_employee', function () {
    let can_add = checkLimit();

    if (!can_add) {
        $("#add_limit_alert_modal").modal('show');
        return false;
    }
});

$(document).on('click', '.delete-icon', function() {
    let employee_id = $(this).attr('data-employee_id');
    $("#delete_employee_id").val(employee_id);
});

$(document).on('click', '#confirm_delete_btn', function () {
    $("#delete_employee_form").submit();
});

$(document).on('submit', '#delete_employee_form', function (e) {
    e.preventDefault();

    let formData = new FormData(this);
    let employee_id = $("#delete_employee_id").val();
    let url = $("#delete_url").val();
    url = url.slice(0,-1);
    url += employee_id;

    $.ajax(
    {
        url: url,
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success:function(output){
            employee_datatable.draw();
            let employee_count = parseInt($("#employee_count").val()) - 1;
            $("#employee_count").val(employee_count);
            $('#delete_modal').modal('toggle');
        }						
    });
});

function checkLimit()
{
    let employee_count = $("#employee_count").val();
    let employee_limit = $("#employee_limit").val();

    if (employee_count == employee_limit) {
        return false;
    }

    return true;
}
</script>
@endsection
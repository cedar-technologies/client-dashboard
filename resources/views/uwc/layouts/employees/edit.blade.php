@extends('uwc.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Update Employee</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('uwc.home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('uwc.employees.view.list') }}">Employees</a></li>
                        <li class="breadcrumb-item active">Update</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <form method="POST" action="{{ route('uwc.employees.update', ['id' => $employee['id']]) }}">
                            @csrf
                            <input type="hidden" name="_method" value="put" />
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="employee_id">Employee ID</label>
                                    <input type="text" class="form-control @error('employee_id') is-invalid @enderror" id="employee_id" name="employee_id" value="{{ $employee['employee_id'] }}" placeholder="E.g. 0123">
                                    @error('employee_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                
                                <div class="form-group">
                                    <label for="name">Full Name</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ $employee['name'] }}" placeholder="E.g. John Doe">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="card_no">Card No</label>
                                    <input type="text" class="form-control @error('card_no') is-invalid @enderror" id="card_no" name="card_no" value="{{ $employee['card_no'] }}" placeholder="E.g. 011-12345678">
                                    @error('card_no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-success"><i class="fas fa-save pr-2"></i>Save</button>
                                <a href="{{ route('uwc.employees.view.list') }}" class="btn btn-dark"><i class="fas fa-times pr-2"></i>Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
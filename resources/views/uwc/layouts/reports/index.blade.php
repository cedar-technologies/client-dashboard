@extends('uwc.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Generate Traceability Excel Report</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('uwc.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Generate Traceability Report</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-8">
                    <div class="card card-success">
                        <form method="POST" action="{{ route('uwc.reports.download') }}">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-6">
                                            <label for="production_line_no">Production Line No</label>
                                            <select class="form-control select2bs4 select2bs4-success @error('production_line_no') is-invalid @enderror" data-dropdown-css-class="select2-success"
                                                name="production_line_no" id="production_line_no" style="width: 100% !important;">
                                                @for ($i = 1; $i <= config('staticdata.production_line_limit'); $i++)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                                <option value="2">2</option>
                                            </select>
                                            @error('production_line_no')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-6">
                                            <label for="station_no">Station No</label>
                                            <div class="form-group">
                                                <select class="form-control select2bs4 select2bs4-success @error('station_no') is-invalid @enderror" data-dropdown-css-class="select2-success"
                                                    name="station_no" id="station_no" style="width: 100% !important;">
                                                    @for ($i = 1; $i <= 3; $i++)
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            @error('station_no')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-6">
                                            <label for="name">From</label>
                                            <input name="start_date" id="start_date" readonly
                                                class="@error('start_date') is-invalid @enderror"
                                                style="background-color: #fff; cursor: pointer">
                                            @error('start_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-6">
                                            <label for="card_no">To</label>
                                            <input name="end_date" id="end_date" readonly
                                                class="@error('end_date') is-invalid @enderror"
                                                style="background-color: #fff; cursor: pointer">
                                            @error('end_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-success"><i class="fas fa-cloud-download-alt pr-2"></i>Download CSV</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const options = { year: 'numeric', month: 'long', day: '2-digit' };
    const default_date = new Date().toLocaleDateString("en-US", options);

    $('.select2bs4').select2();
    $(".select2-selection").eq(2).css({"border-top-right-radius": "0%", "border-bottom-right-radius": "0%"});

    $('#start_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mmmm dd, yyyy',
        value: default_date,
        minDate: new Date('2020-02-24')
    });

    $('#end_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mmmm dd, yyyy',
        value: default_date,
        minDate: new Date('2020-02-24')
    });

</script>
@endsection

@extends('uwc.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Employee Shifts</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('uwc.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Employee Shifts</li>
                    </ol>
                </div>
            </div>

            {{-- Search and Filter --}}
            <div class="row">
                    <div class="col-md-2 col-6">
                        <label for="production_line_dropdown">Production Line:</label>
                        <div class="form-group">
                            <select class="form-control select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                                name="production_line" id="production_line" style="width: 100% !important;">
                                <option value="all">All</option>
                                @for ($i = 1; $i <= config('staticdata.production_line_limit'); $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1 col-6">
                        <label for="station">Station:</label>
                        <div class="form-group">
                            <select class="form-control select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                                name="station" id="station" style="width: 100% !important;">
                                <option value="all">All</option>
                                @for ($i = 1; $i <= 3; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2 col-6">
                        <label for="start_date_range">Date From:</label>
                        <div class="form-group">
                            <input name="start_date_range" id="start_date_range">
                        </div>
                    </div>
                    <div class="col-md-2 col-6">
                        <label for="end_date_range">Date To:</label>
                        <div class="form-group">
                            <input name="end_date_range" id="end_date_range">
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <label for="search_dropdown">Search By:</label>
                        <div class="input-group">
                            <select class="custom-select select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                                name="search_dropdown" id="search_dropdown" style="width: 50%">
                            <option value="" selected disabled>Select Category</option>
                            @foreach (config('staticdata.employee_shifts.search-dropdown') as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                            </select>
    
                            <input type="text" id="keyword" class="form-control" placeholder="Search keyword.." disabled
                                aria-label="Search keyword.." aria-describedby="basic-addon2" style="border-radius: 0%;">
    
                            <div class="input-group-append">
                                <button class="btn btn-success" id="search_btn" type="button" disabled><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-1 col-md-2 col-12">
                        <div class="web-view mt-md-4 pt-md-2 mt-3 pt-2"></div>
                        <div class="form-group">
                            <button class="btn btn-success w-100" id="reset_filter_btn" type="button"><i class="fas fa-undo pr-2"></i>Reset</button>
                        </div>
                    </div>
                </div>

            {{-- Table --}}
            <div class="row">
                <div class="col-12">
                    <div class="card card-success">
                        <div class="card-header text-white bg-success">
                            <h3 class="card-title">Employee Shifts Table</h3>
                        </div>
                        <div class="card-body">
                            <input type="hidden" id="employee_shift_datatable_api_route" value="{{ route('uwc.api.employee-shifts.datatable.list') }}">
                            <table id="employee_shifts_table" class="table table-bordered table-striped table-hover w-100">
                                <thead>
                                    <tr>
                                        <th>Employee ID</th>
                                        <th>PFR No</th>
                                        <th>Production Line</th>
                                        <th>Station</th>
                                        <th>Shift Started At</th>
                                        <th>Shift Ended At</th>
                                        <th>Working Hours</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var employee_shifts_datatable;
var production_line = 1;

$(function() {
    const options = { year: 'numeric', month: 'long', day: '2-digit' };
    const default_start_date = 'January 01, <?php echo \Carbon\Carbon::now()->year ?>';
    const default_end_date = new Date().toLocaleDateString("en-US", options);

    $('.select2bs4').select2();
    $(".select2-selection").eq(2).css({"border-top-right-radius": "0%", "border-bottom-right-radius": "0%"});

    $('#start_date_range').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mmmm dd, yyyy',
        value: default_start_date
    });

    $('#end_date_range').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'mmmm dd, yyyy',
        value: default_end_date
    });

    var employee_shifts_datatable = $('#employee_shifts_table').DataTable({
        "lengthChange": false,
        "searching": false,
        "pageLength": 10,
        "processing": true,
        "serverSide": true,
        "autoWidth": true,
        "scrollX": true,
        "order": [[ 4, "desc" ]],
        "ajax": {
            "url": $("#employee_shift_datatable_api_route").val(),
            "data": function ( data ) {
                data.production_line_no = $("#production_line").val()
                data.station_no = $("#station").val();
                data.start_date = $("#start_date_range").val();
                data.end_date = $("#end_date_range").val();
                data.search_by = $("#search_dropdown").val();
                data.keyword = $("#keyword").val();
            }
        },
        "columns": [
            { data: 'employee_id' },
            { data: 'pfr_no' },
            { data: 'production_line_no' },
            { data: 'station_no' },
            { data: 'clock_in_datetime' },
            { data: 'clock_out_datetime' },
            { data: 'working_hours', orderable: false }
        ]
    });

    $('a[data-widget="pushmenu"]').on( 'click', function (e) {
        setTimeout(function(){
            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
        }, 300);
    });

    $("#search_btn").on('click', function () {
        employee_shifts_datatable.draw();
    })

    $("#reset_filter_btn").on('click', function () {
        $("#start_date_range").val(default_start_date);
        $("#end_date_range").val(default_end_date);
        $("#search_dropdown").val('');
        $("#search_dropdown").change();
        $("#keyword").val('');
        employee_shifts_datatable.draw();
    })
    
    $('#production_line').on('change', function(e) {
        employee_shifts_datatable.draw();
    });
    
    $('#station').on('change', function(e) {
        employee_shifts_datatable.draw();
    });

    $('#keyword').on('keyup', function(e) {
        if (e.which == 13) {
            $("#search_btn").click();
        }
        
        if ($(this).val().length == 0) {
            $("#search_btn").prop('disabled', true);
        } else {
            $("#search_btn").prop('disabled', false);
        }
    });
    
    $('#search_dropdown').on('change', function(e) {
        if (!$(this).val()) {
            $("#keyword").prop('disabled', true);
        } else {
            $("#keyword").prop('disabled', false);
        }
    });
})

</script>
@endsection
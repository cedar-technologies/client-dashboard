@extends('uwc.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-4">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Traceability</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('uwc.home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Traceability</li>
                    </ol>
                </div>
            </div>

            {{-- Search and Filter --}}
            <div class="row">
                <div class="col-md-2 col-12">
                    <label for="production_line_dropdown">Production Line:</label>
                    <div class="form-group">
                        <select class="form-control select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                            name="production_line" id="production_line" style="width: 100% !important;">
                            <option value="all">All</option>
                            @for ($i = 1; $i <= config('staticdata.production_line_limit'); $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-6">
                    <label for="start_date_range">Date From:</label>
                    <div class="form-group">
                        <input name="start_date_range" id="start_date_range">
                    </div>
                </div>
                <div class="col-md-2 col-6">
                    <label for="end_date_range">Date To:</label>
                    <div class="form-group">
                        <input name="end_date_range" id="end_date_range">
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <label for="search_dropdown">Search By:</label>
                    <div class="input-group">
                        <select class="custom-select select2bs4 select2bs4-success" data-dropdown-css-class="select2-success"
                            name="search_dropdown" id="search_dropdown" style="width: 50%">
                        <option value="" selected disabled>Select Category</option>
                        @foreach (config('staticdata.stations.search-dropdown') as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                        </select>

                        <input type="text" id="keyword" class="form-control" placeholder="Search keyword.." disabled
                            aria-label="Search keyword.." aria-describedby="basic-addon2" style="border-radius: 0%;">

                        <div class="input-group-append">
                            <button class="btn btn-success" id="search_btn" type="button" disabled><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-1 col-md-2 col-12">
                    <div class="web-view mt-md-4 pt-md-2 mt-3 pt-2"></div>
                    <div class="form-group">
                        <button class="btn btn-success w-100" id="reset_filter_btn" type="button"><i class="fas fa-undo pr-2"></i>Reset</button>
                    </div>
                </div>
            </div>

            {{-- Tables --}}
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs">
                        <li class="nav-item tab-1">
                            <a class="nav-link active" data-toggle="tab" href="#station1">Station 1</a>
                        </li>
                        <li class="nav-item tab-2">
                            <a class="nav-link" data-toggle="tab" href="#station2">Station 2</a>
                        </li>
                        <li class="nav-item tab-3">
                            <a class="nav-link" data-toggle="tab" href="#station3">Station 3</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="tab-content">
                {{-- Station 1 --}}
                <div class="tab-pane active" id="station1">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-success station">
                                <div class="card-body">
                                    <input type="hidden" id="station1_datatable_api_route" value="{{ route('uwc.api.station-1-jobs.datatable.list') }}">
                                    <table id="station1_table" class="table table-bordered table-striped table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th>Employee ID</th>
                                                <th>Production Line</th>
                                                <th>Body Traceability</th>
                                                <th>Body Subtraceability</th>
                                                <th>Silicon Traceability</th>
                                                <th>PFR 1</th>
                                                <th>Batch Started At</th>
                                                <th>Batch Ended At</th>
                                                <th>Completed units</th>
                                                <th>Incomplete units</th>
                                                <th>Reason of incompletion</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Station 2 --}}
                <div class="tab-pane fade" id="station2">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-success station">
                                <div class="card-body">
                                    <input type="hidden" id="station2_datatable_api_route" value="{{ route('uwc.api.station-2-jobs.datatable.list') }}">
                                    <table id="station2_table" class="table table-bordered table-striped table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th>Employee ID</th>
                                                <th>Production Line</th>
                                                <th>Body Traceability</th>
                                                <th>Body Subtraceability</th>
                                                <th>Cap Traceability</th>
                                                <th>PFR 1</th>
                                                <th>PFR 2</th>
                                                <th>Batch Started At</th>
                                                <th>Batch Ended At</th>
                                                <th>Completed units</th>
                                                <th>Incomplete units</th>
                                                <th>Reason of incompletion</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Station 3 --}}
                <div class="tab-pane fade" id="station3">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-success station">
                                <div class="card-body">
                                    <input type="hidden" id="station3_datatable_api_route" value="{{ route('uwc.api.station-3-jobs.datatable.list') }}">
                                    <table id="station3_table" class="table table-bordered table-striped table-hover w-100">
                                        <thead>
                                            <tr>
                                                <th>Employee ID</th>
                                                <th>Production Line</th>
                                                <th>Body Traceability</th>
                                                <th>Body Subtraceability</th>
                                                <th>Pouch Traceability</th>
                                                <th>Body Tube Traceability</th>
                                                <th>PFR 1</th>
                                                <th>PFR 2</th>
                                                <th>PFR 3</th>
                                                <th>Batch Started At</th>
                                                <th>Batch Ended At</th>
                                                <th>Completed units</th>
                                                <th>Incomplete units</th>
                                                <th>Reason of incompletion</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        const options = { year: 'numeric', month: 'long', day: '2-digit' };
        const default_start_date = 'January 01, <?php echo \Carbon\Carbon::now()->year ?>';
        const default_end_date = new Date().toLocaleDateString("en-US", options);

        $('.select2bs4').select2();
        $(".select2-selection").eq(2).css({"border-top-right-radius": "0%", "border-bottom-right-radius": "0%"});

        $('#start_date_range').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'mmmm dd, yyyy',
            value: default_start_date
        });

        $('#end_date_range').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'mmmm dd, yyyy',
            value: default_end_date
        });

        var station1_table = $('#station1_table').DataTable({
            "lengthChange": false,
            "searching": false,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "order": [[ 4, "desc" ]],
            "ajax": {
                "url": $("#station1_datatable_api_route").val(),
                "data": function ( data ) {
                    data.production_line_no = $("#production_line").val()
                    data.start_date = $("#start_date_range").val();
                    data.end_date = $("#end_date_range").val();
                    data.search_by = $("#search_dropdown").val();
                    data.keyword = $("#keyword").val();
                }
            },
            "columns": [
                { data: 'employee_id' },
                { data: 'production_line_no' },
                { data: 'body_traceability_no' },
                { data: 'body_subtraceability_no' },
                { data: 'silicon_traceability_no' },
                { data: 'pfr_1_no' },
                { data: 'batch_start_datetime' },
                { data: 'batch_end_datetime' },
                { data: 'completed' },
                { data: 'incomplete' },
                { data: 'reason_of_incompletion', orderable: false },
            ]
        });
        
        var station2_table = $('#station2_table').DataTable({
            "lengthChange": false,
            "searching": false,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "order": [[ 6, "desc" ]],
            "ajax": {
                "url": $("#station2_datatable_api_route").val(),
                "data": function ( data ) {
                    data.production_line_no = $("#production_line").val()
                    data.start_date = $("#start_date_range").val();
                    data.end_date = $("#end_date_range").val();
                    data.search_by = $("#search_dropdown").val();
                    data.keyword = $("#keyword").val();
                }
            },
            "columns": [
                { data: 'employee_id' },
                { data: 'production_line_no' },
                { data: 'body_traceability_no' },
                { data: 'body_subtraceability_no' },
                { data: 'cap_traceability_no' },
                { data: 'pfr_1_no' },
                { data: 'pfr_2_no' },
                { data: 'batch_start_datetime' },
                { data: 'batch_end_datetime' },
                { data: 'completed' },
                { data: 'incomplete' },
                { data: 'reason_of_incompletion', orderable: false },
            ]
        });
        
        var station3_table = $('#station3_table').DataTable({
            "lengthChange": false,
            "searching": false,
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "order": [[ 8, "desc" ]],
            "ajax": {
                "url": $("#station3_datatable_api_route").val(),
                "data": function ( data ) {
                    data.production_line_no = $("#production_line").val()
                    data.start_date = $("#start_date_range").val();
                    data.end_date = $("#end_date_range").val();
                    data.search_by = $("#search_dropdown").val();
                    data.keyword = $("#keyword").val();
                }
            },
            "columns": [
                { data: 'employee_id' },
                { data: 'production_line_no' },
                { data: 'body_traceability_no' },
                { data: 'body_subtraceability_no' },
                { data: 'pouch_traceability_no' },
                { data: 'body_tube_traceability_no' },
                { data: 'pfr_1_no' },
                { data: 'pfr_2_no' },
                { data: 'pfr_3_no' },
                { data: 'batch_start_datetime' },
                { data: 'batch_end_datetime' },
                { data: 'completed' },
                { data: 'incomplete' },
                { data: 'reason_of_incompletion', orderable: false },
            ]
        });

        // Tabbed table headers were not sized correctly
        $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
            $($.fn.dataTable.tables( true ) ).css('width', '100%');
            $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
        } );

        $('a[data-widget="pushmenu"]').on( 'click', function (e) {
            setTimeout(function(){
                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
            }, 300);
        } );

        $("#search_btn").on('click', function () {
            station1_table.search( this.value ).draw();
            station2_table.search( this.value ).draw();
            station3_table.search( this.value ).draw();
        })

        $("#reset_filter_btn").on('click', function () {
            $("#start_date_range").val(default_start_date);
            $("#end_date_range").val(default_end_date);
            $("#search_dropdown").val('');
            $("#search_dropdown").change();
            $("#keyword").val('');
            station1_table.search( this.value ).draw();
            station2_table.search( this.value ).draw();
            station3_table.search( this.value ).draw();
        })
        
        $('#production_line').on('change', function(e) {
            station1_table.draw();
            station2_table.draw();
            station3_table.draw();
        });
        
        $('#keyword').on('keyup', function(e) {
            if (e.which == 13) {
                $("#search_btn").click();
            }
            
            if ($(this).val().length == 0) {
                $("#search_btn").prop('disabled', true);
            } else {
                $("#search_btn").prop('disabled', false);
            }
        });

        $('#search_dropdown').on('change', function(e) {
            if (!$(this).val()) {
                $("#keyword").prop('disabled', true);
            } else {
                $("#keyword").prop('disabled', false);
            }
        });  
    })
</script>
@endsection
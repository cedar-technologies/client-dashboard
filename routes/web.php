<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'RootController@index')->name('root');

Route::group(['prefix' => 'uwc', 'namespace' => 'Uwc', 'middleware' => ['auth'], 'as' => 'uwc.'], function () {
    Route::get('/home', 'HomeController@home')->name('home');
    Route::group(['prefix' => 'employees', 'as' => 'employees.'], function () {
        Route::get('/', 'EmployeeController@list')->name('view.list');
        Route::get('create', 'EmployeeController@createView')->name('view.create');
        Route::get('/{id}', 'EmployeeController@edit')->name('view.edit');
        Route::post('/', 'EmployeeController@create')->name('create');
        Route::put('/{id}', 'EmployeeController@update')->name('update');
        Route::delete('/{id}', 'EmployeeController@delete')->name('delete');
    });
    Route::group(['prefix' => 'defects', 'as' => 'defects.'], function () {
        Route::get('/', 'DefectController@list')->name('view.list');
        Route::get('create', 'DefectController@createView')->name('view.create');
        Route::post('/', 'DefectController@createView')->name('create');
    });
    Route::group(['prefix' => 'traceabilities', 'as' => 'stations.'], function () {
        Route::get('/', 'StationController@list')->name('view.list');
    });
    Route::group(['prefix' => 'production-line-analytics', 'as' => 'production-line-analytics.'], function () {
        Route::get('/', 'ProductionLineAnalyticsController@index')->name('view.index');
    });
    Route::group(['prefix' => 'employee-shifts', 'as' => 'employee-shifts.'], function () {
        Route::get('/', 'EmployeeShiftController@list')->name('view.list');
    });
    Route::group(['prefix' => 'reports', 'as' => 'reports.'], function () {
        Route::get('/', 'ReportController@index')->name('view.index');
        Route::post('/', 'ReportController@download')->name('download');
    });
});


Route::group(['prefix' => 'covid19-tracking', 'namespace' => 'Covid19', 'middleware' => ['auth'], 'as' => 'covid19.'], function () {
    Route::get('/home', 'HomeController@home')->name('home');
    Route::get('/users', 'HomeController@userList')->name('users.list');
    Route::get('/user-interactions', 'HomeController@userInteractionList')->name('user-interactions.list');
    Route::get('/infection-tree', 'HomeController@infectionTree')->name('infection-tree');
    Route::get('/news-feed', 'HomeController@newsFeed')->name('news-feed.home');
    Route::get('/hotspot-map', 'HomeController@hotspotMap')->name('hotspot-map.home');
    Route::get('/notification', 'HomeController@notificationList')->name('notification.list');
});
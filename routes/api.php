<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'Uwc', 'as' => 'uwc.'], function () {
    Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {
        Route::group(['prefix' => 'employee-shifts'], function () {
            Route::get('/datatable', 'EmployeeShiftApiController@datatableList')->name('employee-shifts.datatable.list');
        });
        Route::group(['prefix' => 'station-jobs'], function () {
            Route::get('/datatable/1', 'Station1JobApiController@datatableList')->name('station-1-jobs.datatable.list');
            Route::get('/datatable/2', 'Station2JobApiController@datatableList')->name('station-2-jobs.datatable.list');
            Route::get('/datatable/3', 'Station3JobApiController@datatableList')->name('station-3-jobs.datatable.list');
        });
        Route::group(['prefix' => 'defects'], function () {
            Route::get('/datatable', 'DefectApiController@datatableList')->name('defects.datatable.list');
        });
        Route::group(['prefix' => 'charts'], function () {
            Route::get('/daily-percentage', 'ChartApiController@getDailyPercentageChartData')->name('charts.daily-percentage');
            Route::get('/unit-completed', 'ChartApiController@getUnitsCompletedChartData')->name('charts.unit-completed');
            Route::get('/defect', 'ChartApiController@getDefectiveUnitsChartData')->name('charts.defects');
        });
        Route::group(['prefix' => 'employee-analytics'], function () {
            Route::get('/datatable', 'EmployeeAnalyticsApiController@datatableList')->name('employee-analytics.datatable.list');
        });
        Route::group(['prefix' => 'employees'], function () {
            Route::get('/datatable', 'EmployeeManagementApiController@datatableList')->name('employees.datatable.list');
        });
    });
});

Route::group(['namespace' => 'Covid19', 'as' => 'covid19.'], function () {
    Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {
        Route::group(['prefix' => 'statistics', 'as' => 'statistics.'], function () {
            Route::get('/header', 'StatisticApiController@getHeaderData')->name('header-data');
        });
        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
            Route::get('/datatable', 'UserApiController@datatableList')->name('datatable.list');
            Route::post('/update/{id}', 'UserApiController@update')->name('update');
        });
        Route::group(['prefix' => 'user-interactions', 'as' => 'user-interactions.'], function () {
            Route::get('/datatable', 'InteractionApiController@datatableList')->name('datatable.list');
        });
        Route::group(['prefix' => 'charts', 'as' => 'charts.'], function () {
            Route::get('/infected-users', 'ChartApiController@getInfectedUsersChartData')->name('infected-users');
        });
        Route::group(['prefix' => 'infection-tree', 'as' => 'infection-tree.'], function () {
            Route::get('/', 'UserApiController@getInfectionTreeData')->name('data');
        });
        Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function () {
            Route::get('/datatable', 'NotificationApiController@datatableList')->name('datatable.list');
            Route::post('/notify', 'NotificationApiController@updateInteractedUserNotifyFlag')->name('notify');
        });
    });
});
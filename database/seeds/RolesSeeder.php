<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('staticdata.roles') as $key => $value) {
            if (!Role::where('name', $key)->exists()) {
                Role::create(['name' => $key]);
            }
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UwcUsersSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('username', 'admin.uwc')->exists()) {
            User::create(
                [
                    'name' => 'UWC Admin',
                    'username' => 'admin.uwc',
                    'password' => \Hash::make('admin123'),
                    'project' => 'uwc'
                ]
            )->assignRole(config('staticdata.roles.admin'));
        }
        
        if (!User::where('username', 'user.uwc')->exists()) {
            User::create(
                [
                    'name' => 'UWC User',
                    'username' => 'user.uwc',
                    'password' => \Hash::make('user123'),
                    'project' => 'uwc'
                ]
            )->assignRole(config('staticdata.roles.viewer'));
        }
    }
}

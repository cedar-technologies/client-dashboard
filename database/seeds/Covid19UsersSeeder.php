<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class Covid19UsersSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('username', 'admin.covid19')->exists()) {
            User::create(
                [
                    'name' => 'COVID-19 Tracking Admin',
                    'username' => 'admin.covid19',
                    'password' => \Hash::make('admin123'),
                    'project' => 'covid19'
                ]
            )->assignRole(config('staticdata.roles.admin'));
        }
    }
}

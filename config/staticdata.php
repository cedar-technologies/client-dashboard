<?php 

return [
    'avg_units_per_hour' => 400,
    'production_line_limit' => 1,
    'station_limit' => 3,
    'defects' => [
        'table-header' => [
            '0' => 'pfr_no',
            '1' => 'production_line_no',
            '2' => 'station_no',
            '3' => 'object_type',
            '4' => 'traceability_no',
            '5' => 'employee_id',
            '6' => 'created_at',
            '7' => 'reason',
        ],
        'object_type' => [
            'Body' => 'Body',
            'Cap' => 'Cap',
            'Rubber' => 'Rubber',
            'Pouch' => 'Pouch',
            'Body Tube' => 'Body Tube',
            'Other' => 'Other',
        ],
        'search-dropdown' => [
            'pfr_no' => 'PFR No',
            'object_type' => 'Type',
            'traceability_no' => 'Tracebility',
            'employee_id' => 'Reported By',
        ]
    ],
    'stations' => [
        'search-dropdown' => [
            'employee_id' => 'Employee ID',
            'traceability_no' => 'Traceability',
            'pfr_no' => 'PFR No',
        ]
    ],
    'station1' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'production_line_no',
            '2' => 'body_traceability_no',
            '3' => 'body_subtraceability_no',
            '4' => 'silicon_traceability_no',
            '5' => 'pfr_1_no',
            '6' => 'batch_start_datetime',
            '7' => 'batch_end_datetime',
            '8' => 'completed',
            '9' => 'incomplete',
            '10' => 'reason_of_incompletion',
        ]
    ],
    'station2' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'production_line_no',
            '2' => 'body_traceability_no',
            '3' => 'body_subtraceability_no',
            '4' => 'cap_traceability_no',
            '5' => 'pfr_1_no',
            '6' => 'pfr_2_no',
            '7' => 'batch_start_datetime',
            '8' => 'batch_end_datetime',
            '9' => 'completed',
            '10' => 'incomplete',
            '11' => 'reason_of_incompletion',
        ]
    ],
    'station3' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'production_line_no',
            '2' => 'body_traceability_no',
            '3' => 'body_subtraceability_no',
            '4' => 'pouch_traceability_no',
            '5' => 'body_tube_traceability_no',
            '6' => 'pfr_1_no',
            '7' => 'pfr_2_no',
            '8' => 'pfr_3_no',
            '9' => 'batch_start_datetime',
            '10' => 'batch_end_datetime',
            '11' => 'completed',
            '12' => 'incomplete',
            '13' => 'reason_of_incompletion',
        ]
    ],
    'employee_analytics' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'employee_name',
            '2' => 'total_working_hours',
        ]
    ],
    'employee_shifts' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'pfr_no',
            '2' => 'production_line_no',
            '3' => 'station_no',
            '4' => 'clock_in_datetime',
            '5' => 'clock_out_datetime',
            '6' => 'working_hours'
        ],
        'search-dropdown' => [
            'employee_id' => 'Employee ID',
            'pfr_no' => 'PFR No',
        ]
    ],
    'employees' => [
        'table-header' => [
            '0' => 'employee_id',
            '1' => 'name',
            '2' => 'card_no',
            '3' => 'working_hours'
        ],
        'search-dropdown' => [
            'employee_id' => 'Employee ID',
            'name' => 'Full Name',
            'card_no' => 'Card Number',
        ]
    ],
    'employee_limit' => 3,
    'roles' => [
        'admin' => 'admin',
        'viewer' => 'viewer'
    ],
    'months' => [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'Desember',
    ],
    'excel_header' => [
        'station1' => [
            'A' => 20, // PFR
            'B' => 20, // Time - start
            'C' => 20, // Time - end
            'D' => 15, // Performed By
            'E' => 20, // Traceability No
            'F' => 12, // Starting Qty
            'G' => 12, // Accepted Qty
            'H' => 13, // Incomplete - End of shift
            'I' => 18, // Incomplete - Incomplete Product
            'J' => 12, // Incomplete - Other
            'K' => 19, // Rejected - Impurities/Black Dot
            'L' => 10, // Rejected - Dirty
            'M' => 15, // Rejected - Short Material
            'N' => 10, // Rejected - Bubbles
            'O' => 15, // Rejected - Thread Damaged
            'P' => 11, // Rejected - Flow Mark
            'Q' => 16, // Rejected - Excessive Material
            'R' => 13, // Rejected - Body Damage
            'S' => 14, // Rejected - Oil Mark/Oily
            'T' => 17, // Rejected - Rubber Length Out
            'U' => 18, // Rejected - Rubber Location Out
            'V' => 12, // Rejected - QA/Sample
            'W' => 10, // Rejected - Other
            'X' => 10, // Sample - Qty
            'Y' => 16, // Sample - To (location)
            'Z' => 11, // Trans. No
            'AA' => 18, // Review By - Date
            'AB' => 20, // Review By - Name
        ],
        'station2' => [
            'A' => 20, // PFR
            'B' => 20, // Time - start
            'C' => 20, // Time - end
            'D' => 15, // Performed By
            'E' => 20, // Traceability No
            'F' => 20, // Previous Process PFR Insert Rubber No
            'G' => 12, // Starting Qty
            'H' => 12, // Accepted Qty
            'I' => 13, // Incomplete - End of shift
            'J' => 18, // Incomplete - Incomplete Product
            'K' => 12, // Incomplete - Other
            'L' => 19, // Rejected - Impurities/Black Dot
            'M' => 10, // Rejected - Dirty
            'N' => 14, // Rejected - Cap Damaged
            'O' => 14, // Rejected - Body Damaged
            'P' => 14, // Rejected - Oil Mark/Oily
            'Q' => 11, // Rejected - Overpress
            'R' => 18, // Rejected - Press Incomplete
            'S' => 12, // Rejected - QA/Sample
            'T' => 10, // Rejected - Other
            'U' => 10, // Sample - Qty
            'V' => 16, // Sample - To (location)
            'W' => 11, // Trans. No
            'X' => 18, // Review By - Date
            'Y' => 20, // Review By - Name
        ],
        'station3' => [
            'A' => 20, // PFR
            'B' => 20, // Time - start
            'C' => 20, // Time - end
            'D' => 15, // Performed By
            'E' => 20, // Traceability No. (Pouch)
            'F' => 20, // Traceability No. (Body Tube)
            'G' => 20, // Previous Process PFR Insert Cap No.
            'H' => 12, // Starting Qty
            'I' => 12, // Accepted Qty
            'J' => 13, // Incomplete - End of shift
            'K' => 18, // Incomplete - Incomplete Product
            'L' => 12, // Incomplete - Other
            'M' => 19, // Rejected - Impurities/Black Dot
            'N' => 15, // Rejected - Seal Incomplete
            'O' => 15, // Rejected - Seal Damaged
            'P' => 20, // Rejected - Pouch Printing Problem
            'Q' => 19, // Rejected - Lot Printing Problem
            'R' => 12, // Rejected - QA\Sample
            'S' => 10, // Rejected - Other
            'T' => 12, // Use for Lot
            'U' => 10, // Sample - Qty
            'V' => 16, // Sample - To (location)
            'W' => 11, // Trans. No
            'X' => 18, // Review By - Date
            'Y' => 20, // Review By - Name
        ],
    ],

    'covid19' => [
        'users' => [
            'table-header' => [
                '0' => 'mobile_no',
                '1' => 'designation',
                '2' => 'is_covid_positive',
                '3' => 'declared_covid_positive_at',
            ],
            'designation' => [
                '1' => 'Doctor',
                '2' => 'Government Agent',
                '3' => 'User',
            ]
        ],
        'user-interactions' => [
            'table-header' => [
                '0' => 'mobile_no',
                '1' => 'distance',
                '2' => 'min_distance',
                '3' => 'max_distance',
                '4' => 'interaction_started_at',
                '5' => 'duration',
                '6' => 'location',
            ],
        ],
        'notifications' => [
            'table-header' => [
                '0' => 'mobile_no',
                '1' => 'designation',
                '2' => 'declared_covid_positive_at',
            ],
            'designation' => [
                '1' => 'Doctor',
                '2' => 'Government Agent',
                '3' => 'User',
            ],
            'filter' => [
                'duration' => [
                    '1' => 'Last 14 days',
                    '2' => 'Last 1 month',
                    '3' => 'Since first interaction',
                ],
                'interaction_duration' => [
                    '1' => 'Any',
                    '2' => 'More or equal than 10 seconds',
                    '3' => 'More or equal than 1 minute',
                    '4' => 'More or equal than 2 minutes',
                    '5' => 'More or equal than 5 minutes',
                    '6' => 'More or equal than 10 minutes',
                    '7' => 'More or equal than 15 minutes',
                    '8' => 'More or equal than 30 minutes',
                    '9' => 'More or equal than 1 hour',
                ]
            ]
        ],
        'distance_limit' => 15,
    ]
];